
//
//@Component
//@Slf4j
//public class JwtUtils implements Serializable {
//
////	private static final long serialVersionUID = -3234346534536456L;
////private String getJwtSecret = "KSHRDREGISTRATION"	;
////	public static final long JWT_TOKEN_VALIDITY = 86400000;/// 3600 = hour
////	private final String jwtSecret = "KSHRDREGISTRATION";
////	private long jwtExpiration=86400000;/// 3600 = hour
//
//	private static final long serialVersionUID = -3234346534536456L;
//
//	public static final long JWT_TOKEN_VALIDITY = 86400000;/// 3600 = hour
//	private final String jwtSecret = "securitykey";
//	public String generateJwtToken(Authentication authentication){
////		System.out.println("Secret key --> " +jwtSecret);
//		UserDetailImp userPrinciple = (UserDetailImp) authentication.getPrincipal();
//		System.out.println("here is userPrinciple --> " + userPrinciple);
//		return Jwts
//				.builder()
//				.setSubject(userPrinciple.getEmail())
//				.setIssuedAt(new Date())
//				.setExpiration(new Date((new Date()).getTime()+JWT_TOKEN_VALIDITY))
//				.signWith(SignatureAlgorithm.HS512,jwtSecret)
//				.compact();
//
//	}
//
//	// Get usename by the provided token
//	public String getUsernameFromJwtToken(String token){
//
//		if (validateJwtToken(token)){
//
//			return Jwts.parser()
//					.setSigningKey(jwtSecret)
//					.parseClaimsJws(token)
//					.getBody()
//					.getSubject();
//		}else return  null;
//	}
//
//	// create method for validation the jwtToken
//	public boolean validateJwtToken(String authToken){
//
//		try {
//			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
//			return  true;
//		}catch (SignatureException e){ log.error("Invalid JWT signature : {}", e.getMessage());}
//		catch (MalformedJwtException e ){ log.error("Invalid JWT token : {}",e.getMessage());}
//		catch (ExpiredJwtException e){ log.error("JWT token is expired: {}",e.getMessage());}
//		catch (UnsupportedJwtException e){ log.error("JWT token is unsupported : {}",e.getMessage());}
//		catch (IllegalArgumentException e){log.error("JWT claims string is empty : {}",e.getMessage());}
//		catch (Exception e){ log.error("Exception : {}"+ e.getMessage());}
//		return false;
//	}
//
//}


package com.kshrd.storymeapi.security.jwt;

import com.kshrd.storymeapi.security.UserDetailImp;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Component
@Slf4j
public class JwtUtils implements Serializable {

	private static final long serialVersionUID = -3234346534536456L;

	public static final long JWT_TOKEN_VALIDITY = 86400000;/// 3600 = hour
	private final String jwtSecret = "securitykey";

	public String generateJwtToken(Authentication authentication){

		UserDetailImp userPrinciple = (UserDetailImp) authentication.getPrincipal();

		return Jwts
				.builder()
				.setSubject(userPrinciple.getUsername())
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime()+JWT_TOKEN_VALIDITY))
				.signWith(SignatureAlgorithm.HS512,jwtSecret)
				.compact();

	}

	// Get usename by the provided token
	public String getUsernameFromJwtToken(String token){

		if (validateJwtToken(token)){

			return Jwts.parser()
					.setSigningKey(jwtSecret)
					.parseClaimsJws(token)
					.getBody()
					.getSubject();
		}else return  null;
	}

	// create method for validation the jwtToken
	public boolean validateJwtToken(String authToken){

		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return  true;
		}catch (SignatureException e){ log.error("Invalid JWT signature : {}", e.getMessage());}
		catch (MalformedJwtException e ){ log.error("Invalid JWT token : {}",e.getMessage());}
		catch (ExpiredJwtException e){ log.error("JWT token is expired: {}",e.getMessage());}
		catch (UnsupportedJwtException e){ log.error("JWT token is unsupported : {}",e.getMessage());}
		catch (IllegalArgumentException e){log.error("JWT claims string is empty : {}",e.getMessage());}
		catch (Exception e){ log.error("Exception : {}"+ e.getMessage());}
		return false;
	}

}
