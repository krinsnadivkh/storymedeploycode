package com.kshrd.storymeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StorymeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorymeApiApplication.class, args);
    }

}
