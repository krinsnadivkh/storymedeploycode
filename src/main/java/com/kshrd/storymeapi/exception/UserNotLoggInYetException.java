package com.kshrd.storymeapi.exception;

public class UserNotLoggInYetException extends RuntimeException{
    public UserNotLoggInYetException(String message) {
        super(message);
    }
}
