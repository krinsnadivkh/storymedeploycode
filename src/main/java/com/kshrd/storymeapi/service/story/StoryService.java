package com.kshrd.storymeapi.service.story;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.request.story.ParagraphsUpdateRequest;
import com.kshrd.storymeapi.dto.response.story.StoryRespones;
import com.kshrd.storymeapi.dto.response.story.UpdateResponse;
import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import com.kshrd.storymeapi.model.story.Story;
import com.kshrd.storymeapi.model.story.RequestStory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface StoryService {

    int crateStory(@Param("stroy")Story story);
    StoryRespones getAllStory ();

    int getStoryTypeIDByName (String storyType);

    int crateParagraphs(@Param("paragraphs")Paragraphs paragraphs);


    List<GetAllPostByUserIDPayload> getStoryByUserId(String uuid);

    String  getEmotionBYID (int id);

    String getStroyTypeById(int id);
//Create Comment section
    int crateComment(@Param("comment") Comments comment);


//End Create Comment section
int deleteStroy(String storyUuid);

    int updateStroyByUuid(@Param("up") UpdateStoryPayload updateStoryPayload);

    UpdateResponse getStoryByStroyUuid(String uuid);
    int updateParagraphsById(@Param("pa") ParagraphsUpdateRequest paragraphs);


    int updateParagraphsByMyId(@Param("pa") ParagraphsUpdateRequest paragraphs);

    Paragraphs findParagraphById (int id);


    Paragraphs findParagraphByUUid (String uuid);

    int updatePrivacyByUuid(String uuid,Boolean isPublic);

    Story findStroyByUuid(String uuid);
    Story findStroyById(int id);
    int likeStoryPost(int storyId,int userId);

    int unLikePost(int storyId, int userId);
    int isLikePost(int storyId, int userId);

    int countLikeByStoryId(int id);


    GetStroyByUuidPayload getStoryByUuid(String storyUuid);


    int requestStoryType (@Param("sr") RequestStory storyRequest);
    List<RecentpostPayload> getRecentStory();



    int incressView(String uuid, int newveiw);

    int getVeiwCount(String uuid);
    List<SearchStoryPostPayload> searchStoryIbByTitle(String title);

    List<SearchUserPayload> searchUserByName(String name);

    List<SearchStoryPostPayload> searchStoryByType( int typeId);
    List<RecentpostPayload> getPopularStory( );



//v2 sna modify

    List<MemorizeStoryPayload> getMemorizeOneYears(String currentDate,String userUuid);


    List<RecentpostPayload> getAllFollowingStory(int userId );

    List<Integer> getAllFollowingIDByUserId(int userId);


    List<GetAllPostByUserIDPayload> getUserOfTheMonth(String start,String end);

    int insertBestUser(int user_id,int achievementId);

    GetAllPostByUserIDPayload getAllStoryByUserId(int userId);
    List<String> getAllStoryType();

    List<EmotionPayload> getAllEmotion();

    CommentPayload getCommentById(int id);
    GetStroyByUuidPayload getStoryCreateResponse(int id);

}
