package com.kshrd.storymeapi.service.todo_list;

import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;
import com.kshrd.storymeapi.dto.request.todo_list.TodoListRequest;
import com.kshrd.storymeapi.model.todo_list.Todo;
import com.kshrd.storymeapi.model.todo_list.TodoList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TodoListService {

    int createTodoList(@Param("td") TodoList todoList);


    int createTodo(@Param("td") Todo todo);
    Todo findTodoListById(int id);

    Todo findTodoListByUuid(String todoListUuid);

    int updatePrivacyTodoList(String  uuid,boolean privacy);
    Todo findTodoByUuid (String uuid);

    int deleteTodoByUuid(String  uuid);

    boolean isComplete (String todoUuid);

    int updateComplete(String todoUuid,boolean status);
    int deleteTodoListByUuid(String  uuid);

    int updateTodoList (@Param("td") TodoList todoList);

//    v2
List<GetAllTodoListPayload> getAllTodolistByUserUuid(int userID, int limit, int offset);

    GetAllTodoListPayload getTodolistById(int id);
    List<Todo> getTodoByTodoListId(int todoListId);
}

