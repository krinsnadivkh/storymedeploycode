package com.kshrd.storymeapi.service.users;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.request.users.UpdateProfileRequest;
import com.kshrd.storymeapi.model.users.AuthUser;
import com.kshrd.storymeapi.model.users.UserResponse;
import com.kshrd.storymeapi.model.users.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public interface UserService {
//    Optional<UserResponse> findByEmail(String Email);
Users findByEmailFP(String Email);
    Users finduserByresetPasswordToken(String ResetPasswordToken);
   UserResponse findByEmail(String Email);

    AuthUser findUserByEmail(String Email);
    Optional<UserResponse> findByEmailOp(String Email);

    int signUp(Users users);
    int save(Users user);
    int updatePassword(@Param("user") Users user);
    String getTokenByToken(String token);

    Users finduserByUuid (String uuid);

    Users finduserById (int id);

    int followAuthor(int ownerId , int otherUser);

 int getUserIdByEmail (String email);

 int isFollow(int ownerId, int otherUserId);
 int unFollowAuthor(int ownerId, int otherUserId);

 boolean isBan(String uuid);

 List<FollowerPayload> getAllFollowingBy (int userId);

 List<Integer> getFollowingId(int userId);
 FollowingResponePayload getSingleFollowingById(int id);
// FollowingResponePayload getSingleFollowerById(int id);
 int countFollowing(int ownerUuid);

// Follower section
FollowingResponePayload getSingleFollowerById(int id);
 List<Integer> getFollowerId(int userId);
 int countFollower(int ownerUuid);

 int updateUserProfile(@Param("u") UpdateProfileRequest users);
 int getUserIdByUuid (String uuid);
// List<RecentpostPayload> searchStoryIbByTitle(String title);

 UserInfoPayload getUserInfo(String uuid);

 int isBaseUser(int user_id,int achievementId);

// v2 sna who to follow
List<UserInfoPayload> getWhoToFolow();



}
