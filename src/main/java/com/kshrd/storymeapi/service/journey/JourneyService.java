package com.kshrd.storymeapi.service.journey;

import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.model.journey.Journey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface JourneyService {

    int createJourney(@Param("ju") Journey journey);

    Journey findJourneyByUuid(String uuid);

    int updateJourney(@Param("ju") Journey journey);

    int deleteJourney(String uuid);

    int updateJourneyPrivaxy(@Param("ju") Journey journey);
    List<GetAllJourneyPayload> getAllJourney(int limit,int offset);

//    v2
List<GetAllJourneyPayload> getAllJourneyByUserUuid(int userID, int limit,int offset);

    GetAllJourneyPayload getJourneyById(int id);

}
