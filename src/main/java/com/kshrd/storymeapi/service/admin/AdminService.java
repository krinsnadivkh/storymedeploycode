package com.kshrd.storymeapi.service.admin;

import com.kshrd.storymeapi.dto.payload.AdminPayload;
import com.kshrd.storymeapi.dto.payload.GetAllStoryRequestPayload;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminService {


    List<GetAllUserPayload> getAllUser(int limit, int offset);

    int  countAllUser();
    int countAllPost();
    int countAllComment();
    AdminPayload getAdminInfo(String email);

    int banUser(String userUuid,boolean status);

    int getTotalStoryRequest();
    int acceptStoryRequest(String uuid);
    int deleteStoryRequest(String uuid);

   List <GetAllStoryRequestPayload> getAllStoryRequest();


}
