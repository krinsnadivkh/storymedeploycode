package com.kshrd.storymeapi.service.fileservice;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageServiceImp extends FileStorageService {


    public FileStorageServiceImp(FileStorageProperties fileStorageProperties) {
        super(fileStorageProperties);
    }

    @Override
    public String storeFile(MultipartFile file) {
        return null;
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        return null;
    }
}
