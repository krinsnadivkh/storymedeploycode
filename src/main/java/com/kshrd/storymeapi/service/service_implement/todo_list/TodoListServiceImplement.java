package com.kshrd.storymeapi.service.service_implement.todo_list;

import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;
import com.kshrd.storymeapi.dto.request.todo_list.TodoListRequest;
import com.kshrd.storymeapi.model.todo_list.Todo;
import com.kshrd.storymeapi.model.todo_list.TodoList;
import com.kshrd.storymeapi.repository.todo_list.TodoListRepository;
import com.kshrd.storymeapi.service.todo_list.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class TodoListServiceImplement implements TodoListService {
    @Autowired
    TodoListRepository todoListRepository;
    UUID uuid = UUID.randomUUID();


    @Override
    public int createTodoList(TodoList todoList) {
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 30;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
//        todoList.setUuid(String.valueOf(uuid));
        todoList.setUuid(randomString);
        return todoListRepository.createTodoList(todoList);
    }

    @Override
    public int createTodo(Todo todo) {
//        todo.setUuid(String.valueOf(uuid));
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 30;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
        todo.setUuid(randomString);
        return todoListRepository.createTodo(todo);
    }

    @Override
    public Todo findTodoListById(int id) {
        return todoListRepository.findTodoListById(id);
    }

    @Override
    public Todo findTodoListByUuid(String todoListUuid) {
        return todoListRepository.findTodoListByUuid(todoListUuid);
    }

    @Override
    public int updatePrivacyTodoList(String uuid, boolean privacy) {
        return todoListRepository.updatePrivacyTodoList(uuid,privacy);
    }

    @Override
    public Todo findTodoByUuid(String uuid) {
        return todoListRepository.findTodoByUuid(uuid);
    }

    @Override
    public int deleteTodoByUuid(String uuid) {
        return todoListRepository.deleteTodoByUuid(uuid);
    }

    @Override
    public boolean isComplete(String todoUuid) {
        return todoListRepository.isComplete(todoUuid);
    }

    @Override
    public int updateComplete(String todoUuid, boolean status) {
        return todoListRepository.updateComplete(todoUuid,status);
    }

    @Override
    public int deleteTodoListByUuid(String uuid) {
        return todoListRepository.deleteTodoListByUuid(uuid);
    }

    @Override
    public int updateTodoList(TodoList todoList) {
        return todoListRepository.updateTodoList(todoList);
    }

    @Override
    public List<GetAllTodoListPayload> getAllTodolistByUserUuid(int userID, int limit, int offset) {
        return todoListRepository.getAllTodolistByUserUuid(userID,limit,offset);
    }

    @Override
    public GetAllTodoListPayload getTodolistById(int id) {
        return todoListRepository.getTodolistById(id);
    }

    @Override
    public List<Todo> getTodoByTodoListId(int todoListId) {
        return todoListRepository.getTodoByTodoListId(todoListId);
    }


}
