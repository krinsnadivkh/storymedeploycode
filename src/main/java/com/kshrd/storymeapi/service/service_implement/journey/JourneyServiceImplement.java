package com.kshrd.storymeapi.service.service_implement.journey;

import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.model.journey.Journey;
import com.kshrd.storymeapi.repository.journey.JourneyRepository;
import com.kshrd.storymeapi.service.journey.JourneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class JourneyServiceImplement implements JourneyService {
    @Autowired
    JourneyRepository journeyRepository;

    LocalDateTime createAt = LocalDateTime.now();

    @Override
    public int createJourney(Journey journey) {
        UUID uuid = UUID.randomUUID();

        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 30;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
        journey.setUuid(randomString);
        journey.setCreateDate(createAt);
        return journeyRepository.createJourney(journey);
    }

    @Override
    public Journey findJourneyByUuid(String uuid) {
        return journeyRepository.findJourneyByUuid(uuid);
    }

    @Override
    public int updateJourney(Journey journey) {
        return journeyRepository.updateJourney(journey);
    }

    @Override
    public int deleteJourney(String uuid) {
        return journeyRepository.deleteJourney(uuid);
    }

    @Override
    public int updateJourneyPrivaxy(Journey journey) {
        return journeyRepository.updateJourneyPrivaxy(journey);
    }

    @Override
    public List<GetAllJourneyPayload> getAllJourney(int limit,int offset) {
        return journeyRepository.getAllJourney(limit,offset);
    }

    @Override
    public List<GetAllJourneyPayload> getAllJourneyByUserUuid(int userID, int limit, int offset) {
        return journeyRepository.getAllJourneyByUserUuid(userID,limit,offset);
    }

    @Override
    public GetAllJourneyPayload getJourneyById(int id) {
        return journeyRepository.getJourneyById(id);
    }
//v2


}
