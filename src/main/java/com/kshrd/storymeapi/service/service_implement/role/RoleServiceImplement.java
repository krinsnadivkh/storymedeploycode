package com.kshrd.storymeapi.service.service_implement.role;

import com.kshrd.storymeapi.repository.roles.RolesRepository;
import com.kshrd.storymeapi.service.role.RoleSerivice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImplement implements RoleSerivice {
    @Autowired
    RolesRepository rolesRepository;

    @Override
    public int insertUserRole(int userId, int roleId) {
        return rolesRepository.insertUserRole(userId,roleId);
    }
}
