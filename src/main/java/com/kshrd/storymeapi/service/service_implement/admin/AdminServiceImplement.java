package com.kshrd.storymeapi.service.service_implement.admin;


import com.kshrd.storymeapi.dto.payload.AdminPayload;
import com.kshrd.storymeapi.dto.payload.GetAllStoryRequestPayload;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;
import com.kshrd.storymeapi.repository.admin.AdminRepository;
import com.kshrd.storymeapi.service.admin.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImplement implements AdminService {
@Autowired
    AdminRepository adminRepository;


    @Override
    public List<GetAllUserPayload> getAllUser(int limit, int offset) {
        return adminRepository.getAllUser(limit,offset);
    }

    @Override
    public int  countAllUser() {
        return adminRepository.countAllUser();
    }

    @Override
    public int countAllPost() {
        return adminRepository.countAllPost();
    }

    @Override
    public int countAllComment() {
        return adminRepository.countAllComment();
    }

    @Override
    public AdminPayload getAdminInfo(String email) {
        return adminRepository.getAdminInfo(email);
    }

    @Override
    public int banUser(String userUuid, boolean status) {
        return adminRepository.banUser(userUuid,status);
    }

    @Override
    public int getTotalStoryRequest() {
        return adminRepository.getTotalStoryRequest();
    }

    @Override
    public int acceptStoryRequest(String uuid) {
        return adminRepository.acceptStoryRequest(uuid);
    }

    @Override
    public int deleteStoryRequest(String uuid) {
        return adminRepository.deleteStoryRequest(uuid);
    }

    @Override
    public List<GetAllStoryRequestPayload> getAllStoryRequest() {
        return adminRepository.getAllStoryRequest();
    }


}
