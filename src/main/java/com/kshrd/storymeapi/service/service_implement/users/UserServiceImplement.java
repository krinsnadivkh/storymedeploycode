package com.kshrd.storymeapi.service.service_implement.users;


import com.kshrd.storymeapi.dto.payload.FollowerPayload;
import com.kshrd.storymeapi.dto.payload.FollowingResponePayload;
//import com.kshrd.storymeapi.dto.payload.UserResponePayload;
import com.kshrd.storymeapi.dto.payload.RecentUser;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;
import com.kshrd.storymeapi.dto.request.users.UpdateProfileRequest;
import com.kshrd.storymeapi.model.users.AuthUser;
import com.kshrd.storymeapi.model.users.UserResponse;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.repository.users.UserRepository;
import com.kshrd.storymeapi.service.users.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImplement implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;


//    @Override
//    public Optional<UserResponse> findByEmail(String Email) {
//        return userRepository.findByEmail(Email);
//    }


    @Override
    public Users findByEmailFP(String Email) {
        return userRepository.findByEmailFP(Email);
    }

    @Override
    public Users finduserByresetPasswordToken(String ResetPasswordToken) {
        return userRepository.finduserByresetPasswordToken(ResetPasswordToken);
    }

    @Override
    public UserResponse findByEmail(String Email) {
        return userRepository.findByEmail(Email);
    }

    @Override
    public AuthUser findUserByEmail(String Email) {
        return userRepository.findUserByEmail(Email);
    }

    @Override
    public Optional<UserResponse> findByEmailOp(String Email) {
        return userRepository.findByEmailOp(Email);
    }

    @Override
    public int signUp(Users users) {
        String encryptedPassword = passwordEncoder.encode(users.getPassword());
        UUID uuid = UUID.randomUUID();
        users.setPassword(encryptedPassword);
        users.setUuid(uuid.randomUUID());
        System.out.println("Here is UUID"+users.getUuid());
      return userRepository.signUp(users);
    }

    @Override
    public int save(Users user) {
        return userRepository.save(user);
    }

    @Override
    public int updatePassword(Users user) {
        return userRepository.updatePassword(user);
    }

    @Override
    public String getTokenByToken(String token) {
        return userRepository.getTokenByToken(token);
    }

    @Override
    public Users finduserByUuid(String uuid) {
        return userRepository.finduserByUuid(uuid);
    }

    @Override
    public Users finduserById(int id) {
        return userRepository.finduserById(id);
    }

    @Override
    public int followAuthor(int ownerId, int otherUser) {
        return userRepository.followAuthor(ownerId,otherUser);
    }

    @Override
    public int getUserIdByEmail(String email) {
        return userRepository.getUserIdByEmail(email);
    }

    @Override
    public int isFollow(int ownerId, int otherUserId) {
        return userRepository.isFollow(ownerId,otherUserId);
    }

    @Override
    public int unFollowAuthor(int ownerId, int otherUserId) {
        return userRepository.unFollowAuthor(ownerId,otherUserId);
    }

    @Override
    public boolean isBan(String uuid) {
        return userRepository.isBan(uuid);
    }

    @Override
    public List<FollowerPayload> getAllFollowingBy(int userId) {
        return userRepository.getAllFollowingBy(userId);
    }

    @Override
    public List<Integer> getFollowingId(int userId) {
        return userRepository.getFollowingId(userId);
    }

    @Override
    public FollowingResponePayload getSingleFollowingById(int id) {
        return userRepository.getSingleFollowingById(id);
    }


//    @Override
//    public FollowingResponePayload getSingleFollowerById(int id) {
//        return userRepository.getSingleFollowerById(id);
//    }

    @Override
    public int countFollowing(int ownerUuid) {
        return userRepository.countFollowing(ownerUuid);
    }

    @Override
    public FollowingResponePayload getSingleFollowerById(int id) {
        return userRepository.getSingleFollowerById(id);
    }

    @Override
    public List<Integer> getFollowerId(int userId) {
        return userRepository.getFollowerId(userId);
    }

    @Override
    public int countFollower(int ownerUuid) {
        return userRepository.countFollower(ownerUuid);
    }

    @Override
    public int updateUserProfile(UpdateProfileRequest users) {
        return userRepository.updateUserProfile(users);
    }

    @Override
    public int getUserIdByUuid(String uuid) {
        return userRepository.getUserIdByUuid(uuid);
    }

    @Override
    public UserInfoPayload getUserInfo(String uuid) {
        return userRepository.getUserInfo(uuid);
    }

    @Override
    public int isBaseUser(int user_id, int achievementId) {
        return userRepository.isBaseUser(user_id,achievementId);
    }

    @Override
    public List<UserInfoPayload> getWhoToFolow() {
        return userRepository.getWhoToFolow();
    }


}
