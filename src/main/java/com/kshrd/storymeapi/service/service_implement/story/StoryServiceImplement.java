package com.kshrd.storymeapi.service.service_implement.story;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.request.story.ParagraphsUpdateRequest;
import com.kshrd.storymeapi.dto.response.story.StoryRespones;
import com.kshrd.storymeapi.dto.response.story.UpdateResponse;
import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import com.kshrd.storymeapi.model.story.Story;
import com.kshrd.storymeapi.model.story.RequestStory;
import com.kshrd.storymeapi.repository.story.StoryRepository;
import com.kshrd.storymeapi.repository.users.UserRepository;
import com.kshrd.storymeapi.service.story.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;


@Service
public class StoryServiceImplement implements StoryService
{
    @Autowired
    StoryRepository storyRepository;
    @Autowired
    UserRepository userRepository;
    UUID uuid = UUID.randomUUID();

    LocalDateTime createAt = LocalDateTime.now();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    @Override
    public int crateStory(Story story) {
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 30;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
//story.setUuid(UUID.fromString(randomString));
        story.setUuid(randomString);
//        story.setUuid(uuid);
        story.setCreateDate(formatter.format(date));

        return storyRepository.crateStory(story);
    }

    @Override
    public StoryRespones getAllStory () {
        return storyRepository.getAllStory();
    }

    @Override
    public int getStoryTypeIDByName(String storyType) {
        return storyRepository.getStoryTypeIDByName(storyType);
    }

    @Override
    public int crateParagraphs(Paragraphs paragraphs) {
//        paragraphs.setUuid(uuid);
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 30;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
//        paragraphs.setUuid(randomString);
        paragraphs.setUuid(randomString);
        return storyRepository.crateParagraphs(paragraphs);
    }

    @Override
    public List<GetAllPostByUserIDPayload> getStoryByUserId(String uuid) {
        return storyRepository.getStoryByUserId(uuid);
    }


//    @Override
//    public List<GetAllPostByUserIDPayload> getStoryByUserId(int id) {
//        return storyRepository.getStoryByUserId(id);
//    }

    @Override
    public String getEmotionBYID(int id) {
        return storyRepository.getEmotionBYID(id);
    }

    @Override
    public String getStroyTypeById(int id) {
        return storyRepository.getStroyTypeById(id);
    }

    @Override
    public int crateComment(Comments comment) {
        comment.setUuid(uuid);
        comment.setCreateDate(createAt);
        return storyRepository.crateComment(comment);
    }

    @Override
    public int deleteStroy(String storyUuid) {
        return storyRepository.deleteStroy(storyUuid);
    }

    @Override
    public int updateStroyByUuid(UpdateStoryPayload updateStoryPayload) {
        return storyRepository.updateStroyByUuid(updateStoryPayload);
    }

    @Override
    public UpdateResponse getStoryByStroyUuid(String uuid) {
        return storyRepository.getStoryByStroyUuid(uuid);
    }

    @Override
    public int updateParagraphsById(ParagraphsUpdateRequest paragraphs) {
        return storyRepository.updateParagraphsById(paragraphs);
    }

    @Override
    public int updateParagraphsByMyId(ParagraphsUpdateRequest paragraphs) {
        return storyRepository.updateParagraphsByMyId(paragraphs);
    }

    @Override
    public Paragraphs findParagraphById(int id) {
        return storyRepository.findParagraphById(id);
    }

    @Override
    public Paragraphs findParagraphByUUid(String uuid) {
        return storyRepository.findParagraphByUUid(uuid);
    }

    @Override
    public int updatePrivacyByUuid(String uuid, Boolean isPublic) {
        return storyRepository.updatePrivacyByUuid(uuid,isPublic);
    }

    @Override
    public Story findStroyByUuid(String uuid) {
        return storyRepository.findStroyByUuid(uuid);
    }

    @Override
    public Story findStroyById(int id) {
        return storyRepository.findStroyById(id);
    }

    @Override
    public int likeStoryPost(int storyId, int userId) {
        return storyRepository.likeStoryPost(storyId,userId);
    }

    @Override
    public int unLikePost(int storyId, int userId) {
        return storyRepository.unLikePost(storyId,userId);
    }

    @Override
    public int isLikePost(int storyId, int userId) {
        return storyRepository.isLikePost(storyId,userId);
    }

    @Override
    public int countLikeByStoryId(int id) {
        return storyRepository.countLikeByStoryId(id);
    }


    @Override
    public GetStroyByUuidPayload getStoryByUuid(String storyUuid) {
        return storyRepository.getStoryByUuid(storyUuid);
    }

    @Override
    public int requestStoryType(RequestStory storyRequest) {
        storyRequest.setUuid(String.valueOf(uuid));
        return storyRepository.requestStoryType(storyRequest);
    }

    @Override
    public List<RecentpostPayload> getRecentStory( ) {
        return storyRepository.getRecentStory();
    }

    @Override
    public int incressView(String uuid, int newveiw){
        return storyRepository.incressView(uuid,newveiw);
    }

    @Override
    public int getVeiwCount(String uuid) {
        return storyRepository.getVeiwCount(uuid);
    }

    @Override
    public List<SearchStoryPostPayload> searchStoryIbByTitle(String title) {
        return storyRepository.searchStoryIbByTitle(title);
    }

    @Override
    public List<SearchUserPayload> searchUserByName(String name) {
        return storyRepository.searchUserByName(name);
    }

    @Override
    public List<SearchStoryPostPayload> searchStoryByType(int typeId) {
        return storyRepository.searchStoryByType(typeId);
    }

    @Override
    public List<RecentpostPayload> getPopularStory() {
        return storyRepository.getPopularStory();
    }

    @Override
    public List<MemorizeStoryPayload> getMemorizeOneYears(String currentDate, String userUuid) {
        return storyRepository.getMemorizeOneYears(currentDate,userUuid);
    }

    @Override
    public List<RecentpostPayload> getAllFollowingStory(int userId) {
        return storyRepository.getAllFollowingStory(userId);
    }

    @Override
    public List<Integer> getAllFollowingIDByUserId(int userId) {
        return storyRepository.getAllFollowingIDByUserId(userId);
    }



    @Override
    public List<GetAllPostByUserIDPayload> getUserOfTheMonth(String start, String end) {
        return storyRepository.getUserOfTheMonth(start,end);
    }

    @Override
    public int insertBestUser(int user_id, int achievementId) {
        return storyRepository.insertBestUser(user_id,achievementId);
    }

    @Override
    public GetAllPostByUserIDPayload getAllStoryByUserId(int userId) {
        return storyRepository.getAllStoryByUserId(userId);
    }

    @Override
    public List<String> getAllStoryType() {
        return storyRepository.getAllStoryType();
    }

    @Override
    public List<EmotionPayload> getAllEmotion() {
        return storyRepository.getAllEmotion();
    }

    @Override
    public CommentPayload getCommentById(int id) {
        return storyRepository.getCommentById(id);
    }

    @Override
    public GetStroyByUuidPayload getStoryCreateResponse(int id) {
        return storyRepository.getStoryCreateResponse(id);
    }


//    version 2 modify



//    @Override
//    public UpdateStoryRespone getStoryByStroyUuid(String uuid) {
//        return storyRepository.getStoryByStroyUuid(uuid);
//    }


//    @Override
//    public Users finduserByUuid(String uuid) {
//        return userRepository.finduserByUuid(uuid);
//    }


}
