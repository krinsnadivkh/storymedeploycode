package com.kshrd.storymeapi.service.role;

import org.springframework.stereotype.Service;

@Service
public interface RoleSerivice {
    int insertUserRole(int userId, int roleId);
}
