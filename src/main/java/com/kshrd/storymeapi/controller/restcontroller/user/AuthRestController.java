package com.kshrd.storymeapi.controller.restcontroller.user;

import com.kshrd.storymeapi.dto.payload.ErrorPayload;
import com.kshrd.storymeapi.dto.payload.UserPayload;
import com.kshrd.storymeapi.dto.payload.VerifyTokenPayload;
import com.kshrd.storymeapi.dto.request.users.UserForgotPasswordRequest;
import com.kshrd.storymeapi.dto.request.users.UserLoginRequest;
import com.kshrd.storymeapi.dto.request.users.UserRegisterRequest;
import com.kshrd.storymeapi.dto.request.users.UserResetPasswordRequest;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.users.UserLoginResponse;
import com.kshrd.storymeapi.dto.response.users.UserLogingResponse;

import com.kshrd.storymeapi.dto.response.users.UserRegisterResponse;
import com.kshrd.storymeapi.dto.response.users.VerifyTokenResponse;
import com.kshrd.storymeapi.model.users.UserResponse;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.security.jwt.JwtUtils;
import com.kshrd.storymeapi.service.EmailService;
import com.kshrd.storymeapi.service.role.RoleSerivice;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.CustomMessage;
import com.kshrd.storymeapi.utilities.Utility;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import net.bytebuddy.utility.RandomString;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/auth")
@SecurityRequirement(name="openapi")
public class AuthRestController {
UserLogingResponse logingResponse;


ModelMapper modelMapper = new ModelMapper();
@Autowired
private UserService userService;
@Autowired
    RoleSerivice roleSerivice;
@Autowired
private AuthenticationManager authenticationManager;

@Autowired
private PasswordEncoder passwordEncoder;

Date requestTime = new Date();

@Autowired
    EmailService emailService;
private boolean verifyTokenStatus;
    List<ErrorPayload> errorsMessage = new ArrayList<>();

    @PostMapping("/login")
    @Operation(summary = "Login")
    ResponseEntity<?> login( @RequestBody @Valid UserLoginRequest request) {
        List<ErrorPayload> errors = new ArrayList<>();
        UserPayload response = new UserPayload();
        UserPayload userResponses = new  UserPayload();
        if (request.getEmail().isEmpty()||request.getPassword().isEmpty())
        {
//            if((!request.getEmail().endsWith("@gmail.com")))
//            {
//                System.out.println("she hello");
//            }
             if(request.getEmail().isEmpty())
            {
                errors.add(new ErrorPayload("Email","Email can not be empty!!!"));
                return new ResponseEntity<>(new ErrorResponse(204,"Please Enter Your Email  Before Login!!!",requestTime,errors), HttpStatus.REQUEST_TIMEOUT);
            }
            else if (request.getPassword().isEmpty())
            {
                errors.add(new ErrorPayload("Password","Password can not be empty!!!"));
                return new ResponseEntity<>(new ErrorResponse(204,"Please Enter Your Password Before Login!!!",requestTime,errors), HttpStatus.REQUEST_TIMEOUT);
            }
            else if (request.getEmail().isEmpty()&&request.getPassword().isEmpty())
            {
                errors.add(new ErrorPayload("Password & Email","Password and Email can not be empty!!!"));
                return new ResponseEntity<>(new ErrorResponse(204,"Please Enter Your Password and Email Before Loging  Before Login!!!",requestTime,errors), HttpStatus.REQUEST_TIMEOUT);
            }
        }else {
            try {
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(
                                request.getEmail(), request.getPassword()
                        );
                Authentication authentication = authenticationManager.authenticate(authenticationToken);
                JwtUtils jwtUtils = new JwtUtils();
                // jwtToken
                String token = jwtUtils.generateJwtToken(authentication);
//            System.out.println("here is token --> " + token);

                UserResponse findUserByEmail = userService.findByEmail(request.getEmail());
                if(findUserByEmail.getStatus().equals("f"))
                {
                    errors.add(new ErrorPayload("Login","User have been ban!!!"));
                    return new ResponseEntity<>(new ErrorResponse(404,"Unfortunately you accont have been ban !!!",requestTime,errors), HttpStatus.NOT_FOUND);
                }
                else {

//                    System.out.println("here is  findUserByEmail " + findUserByEmail);
                    userResponses = modelMapper.map(userService.findUserByEmail(request.getEmail()), UserPayload.class);
                    userResponses.setToken(token);
                }

                errors.add(new ErrorPayload("Login", "Error when query database!"));


            } catch (Exception exception) {
                System.out.println("Exception occur when trying to login for this credential : " + exception.getMessage());
                errors.add(new ErrorPayload("Login", "Error occur when trying to login"));
                return new ResponseEntity<>(new ErrorResponse(406, "Wrong Password Or Email please try again ", requestTime, errors), HttpStatus.BAD_REQUEST);
            }
        }



        return new ResponseEntity<>(new UserLoginResponse(200, "Login successfully", requestTime,  userResponses), HttpStatus.OK);
    }




    @PostMapping("/singUp")
    @Operation(summary = "SingUp")
    public ResponseEntity<?> registerNewUser(@RequestBody @Valid UserRegisterRequest user) {
        List<ErrorPayload> errors = new ArrayList<>();
        LocalDateTime createDate = LocalDateTime.now();
        AtomicBoolean isExist = new AtomicBoolean(false);
        try {
            userService.findByEmailOp(user.getEmail()).ifPresent(name -> {
                errors.add(new ErrorPayload("Email", "email already exist!"));
                isExist.set(true);
            });
            if(isExist.get()){
                return new ResponseEntity<>(new ErrorResponse(406, CustomMessage.VALIDATION_ERROR, requestTime, errors), HttpStatus.BAD_REQUEST);
            }


            Users user1 = new Users();
            user1.setEmail(user.getEmail());
            user1.setPassword(user.getPassword());
            user1.setFull_name(user.getFull_name());
            user1.setGender(user.getGender());
            user1.setDate_of_birth(user.getDate_of_birth());
            user1.setCreateDate(createDate);


            int newInsertedId = userService.signUp(user1);
            //insert user_role
          roleSerivice.insertUserRole(newInsertedId, 1); //role_user
            System.out.println("here is insert id: " + newInsertedId);
        } catch (Exception ex) {
            System.out.println("Insert user Exception ; " + ex.getMessage());
            return new ResponseEntity<>(new UserRegisterResponse(400,ex.getMessage(),requestTime,user),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Register successfully ",requestTime),HttpStatus.CREATED);
    }

    @PatchMapping("/forgot-password")
    @Operation(summary = "forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestBody @Valid UserForgotPasswordRequest forgotPasswordRequest, HttpServletRequest request) {
        String email = forgotPasswordRequest.getEmail();
        String token = RandomString.make(6);
        List<ErrorPayload> errors = new ArrayList<>();
        try{
                Users user = userService.findByEmailFP(email);
                if (user != null) {
                    user.setResetPasswordToken(token);
                    user.setEmail(email);
                    userService.save(user);
                    String resetPasswordLink ="<p>Hello,</p>"
                            + "<p>You have requested to reset your password.</p>"
                            + "<p>Here is your verify </p>"
                            + "<h2 style='color:#069A8E;text-align:center;'>" +
                            token+ "</h2>"
                            + "<br>"
                            + "<p style='color:red;'>Ignore this email if you do remember your password, "
                            + "or you have not made the request.</p>"  ;
                    System.out.println("Here is the reset passwrod likm"+ resetPasswordLink);
                    emailService.sendByMail(email,resetPasswordLink);


                } else {
                    errors.add(new ErrorPayload("User", "Can not find any user with this email"));
                    return new ResponseEntity<>(new ErrorResponse(400, CustomMessage.USER_NOT_FOUND, requestTime, errors), HttpStatus.BAD_REQUEST);
                }




        }catch (Exception e)
        {
            return new ResponseEntity<>(new ErrorResponse(406, CustomMessage.USER_NOT_FOUND, requestTime, errors), HttpStatus.BAD_REQUEST);
        }


        return new ResponseEntity<>(new SuccessResponse(201,"We have send Reset Password to you email. Please check your email. ",requestTime),HttpStatus.CREATED);
}





String tmpToken;

        @PatchMapping("/verify-Token")
    @Operation(summary = "verify-Token")
    public ResponseEntity<?> verifyToken(@RequestParam String token) {
            errorsMessage.removeAll(errorsMessage);
            tmpToken=token;
            System.out.println("Here is "+ token);
            VerifyTokenPayload verifyTokenPayload = new VerifyTokenPayload();
        try{
            String systemToken  =userService.getTokenByToken(token);
            System.out.println("Here  is system "+ systemToken);
            if(systemToken!=null)
            {
                if(systemToken.equals(token))
                {
                    System.out.println("match");
                    verifyTokenStatus=true;
                    verifyTokenPayload.setStatus(true);
                    return new ResponseEntity<>(new VerifyTokenResponse(201,"Token Match...",requestTime,verifyTokenPayload),HttpStatus.FOUND);
//                    return new ResponseEntity<>(new SuccessResponse(201,"Token Match ...",requestTime),HttpStatus.CREATED);

                }
                else {
//                    errorsMessage.add(new ErrorPayload("Token", "No any user with this token in our website"));
                    verifyTokenStatus=false;
//                    return new ResponseEntity<>(new ErrorResponse(406, "Invalid Token", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
                }

            }
            else {
                errorsMessage.add(new ErrorPayload("Token", "No any user with this token in our website"));
                verifyTokenStatus=false;
//                return new ResponseEntity<>(new ErrorResponse(406, "Invalid Token", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Nice to see error");
            return new ResponseEntity<>(new ErrorResponse(406, "Can not find any user with this token !!!", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
        }
            errorsMessage.add(new ErrorPayload("Token", "There are some problem while trying lo verifiToken"));
            verifyTokenStatus=false;
                return new ResponseEntity<>(new ErrorResponse(406, "Invalid Token", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
      }





    @PatchMapping("/reset-password")
    @Operation(summary = "reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody @Valid UserResetPasswordRequest userResetPasswordRequest, HttpServletRequest request) {
        errorsMessage.removeAll(errorsMessage);
        String password = userResetPasswordRequest.getPassword();
        String comfirmPassword = userResetPasswordRequest.getComfrimePassword();

        System.out.println("Here is the verifyTokenStatus"+ verifyTokenStatus);
        try{
            Users findUser=userService.finduserByresetPasswordToken(tmpToken);
            Users user= new Users();
            if(verifyTokenStatus==true)
            {
                if (password.equals(comfirmPassword))
                {
                    String encryptedPassword = passwordEncoder.encode(password);
                    user.setEmail(findUser.getEmail());
                    user.setPassword(encryptedPassword);
                    user.setResetPasswordToken(null);
                    userService.updatePassword(user);
                }
                else {
                    errorsMessage.add(new ErrorPayload("ComfirmPassword", "Comfirm Password Not Match Please try again !!!"));
                    return new ResponseEntity<>(new ErrorResponse(404, "No useer with thes email in stroyMe", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
                }
            }
            else {
                errorsMessage.add(new ErrorPayload("Token", "Token was Expired or Invalid"));
                return new ResponseEntity<>(new ErrorResponse(404, "Wrong Token", requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
//                System.out.println("Token Invalid  ");

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errorsMessage.add(new ErrorPayload("User", "Cannot find anyuser with this token !!!"));
            return new ResponseEntity<>(new ErrorResponse(404, CustomMessage.USER_NOT_FOUND, requestTime, errorsMessage), HttpStatus.BAD_REQUEST);
        }


        return new ResponseEntity<>(new SuccessResponse(201,"Password Reset successfully ",requestTime),HttpStatus.CREATED);
    }



}
