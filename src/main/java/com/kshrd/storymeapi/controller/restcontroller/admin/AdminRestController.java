package com.kshrd.storymeapi.controller.restcontroller.admin;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.admin.AdminInfoResponse;
import com.kshrd.storymeapi.dto.response.admin.GetAllStoryRequestResponse;
import com.kshrd.storymeapi.dto.response.admin.GetAllUserResponse;
import com.kshrd.storymeapi.dto.response.admin.TotalUserRespone;
import com.kshrd.storymeapi.dto.response.story.GetAllPostByUserIDRespone;
import com.kshrd.storymeapi.dto.response.story.GetStroyByUuidRespone;
import com.kshrd.storymeapi.dto.response.users.SearchUserByNameResponse;
import com.kshrd.storymeapi.dto.response.users.UserInfoResponse;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.service.admin.AdminService;
import com.kshrd.storymeapi.service.story.StoryService;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.customfuntion.CustomFuntion;
import com.kshrd.storymeapi.utilities.paging.Paging;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**********************************************************************
 * Original Author: Try Krisna
 * Created Date: 30/05/2022
 * Development Group: StoryMe Group
 * Description: AdminController Class
 **********************************************************************/

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/admin")
public class AdminRestController {
    Date requestTime = new Date();
    List<ErrorPayload> errors = new ArrayList<>();

    @Autowired
    AdminService adminService;
    @Autowired
    UserService userService;
    @Autowired
    StoryService storyService;


    @GetMapping("get-admin-info")
    @Operation(summary = "get admin info")
    ResponseEntity<?> getAdminInfo() {
        String msg= "";
        String curentUserEmail;
        CustomFuntion customFuntion= new CustomFuntion();
        AdminPayload adminPayload= new AdminPayload();
        try {
            curentUserEmail=customFuntion. getCurrentUser();
            System.out.println("user Email"+ curentUserEmail);

            if(curentUserEmail=="anonymousUser")
            {
                errors.add(new ErrorPayload("Login Required","You have  not login you can not access this resource !!!"));
                return new ResponseEntity<>(new ErrorResponse(404,"Not allow ",requestTime,errors),HttpStatus.METHOD_NOT_ALLOWED);
            }
            else {
                adminPayload=adminService.getAdminInfo(curentUserEmail);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorResponse(404,"There are problem while fetching data !!!",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new AdminInfoResponse(202,"Admin info fetch successfully...",requestTime,adminPayload),HttpStatus.FOUND);
    }


    @GetMapping("story-total-story-request")
    @Operation(summary = "Get Total Story Request")
    ResponseEntity<?> requestNotification() {
        int totalStoryRequest = 0;
        String msg= "";
        try
        {
            totalStoryRequest =adminService. getTotalStoryRequest();
            if(totalStoryRequest==0){
                msg="There are No Story Request";
            }
            msg="Get total story type Request successfully";
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new TotalUserRespone(202,msg,requestTime,totalStoryRequest),HttpStatus.FOUND);

    }



    @PatchMapping("accept-story-request")
    @Operation(summary = "accept story request")
    ResponseEntity<?> acceptStoryRequest(@RequestParam @Valid String storyTypeUuid) {
        String msg= "";
        try
        {
            int result =adminService. acceptStoryRequest(storyTypeUuid);
            System.out.println(result);
            if(result==0){
                msg="There are No Story Request";
            }
            msg="Get total story type Request successfully";
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(202,"Story was accept successfully",requestTime),HttpStatus.FOUND);

    }






    @GetMapping("get-total-users")
    @Operation(summary = "get all total user ")
    ResponseEntity<?> getTotalUsers() {
        int totalUser = 0;
        String msg= "";
        try
        {
           totalUser =adminService. countAllUser();
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new TotalUserRespone(202,"Total User",requestTime,totalUser),HttpStatus.FOUND);
    }


    @GetMapping("get-total-Post")
    @Operation(summary = "count total post")
    ResponseEntity<?> getTotalPost() {
        int totalPost = 0;
        try
        {
            totalPost =adminService.countAllPost();
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new TotalUserRespone(202,"Total Post",requestTime,totalPost),HttpStatus.FOUND);
    }


    @GetMapping("get-total-comment")
    @Operation(summary = "count total comment")
    ResponseEntity<?> getTotalComment() {
        int totalComment = 0;
        try
        {
            totalComment =adminService.countAllComment();
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new TotalUserRespone(202,"Total comment",requestTime,totalComment),HttpStatus.FOUND);
    }


    @PatchMapping("ban-and-unban-user")
    @Operation(summary = "Ban and UnBan user")
    ResponseEntity<?> banAndUnbanUser(@RequestParam @Valid String userUuid) {
        errors.removeAll(errors);
        boolean status = false;
        String msg= "";
        System.out.println("Here is the user uuid"+ userUuid);
        int updateResult ;
    Users isFoundUser =    userService.finduserByUuid(userUuid);

    if(isFoundUser==null)
    {

        errors.add(new ErrorPayload("User","cannot find any user with this uuid !!!"));
        return new ResponseEntity<>(new ErrorResponse(403,"Failed to perform action",requestTime,errors),HttpStatus.BAD_REQUEST);
    }
    else {

        try{        if(userService.isBan(userUuid)==true){
            updateResult = adminService.banUser(userUuid, false);
            msg="Ban user successfully...";
        }
        else {
            updateResult = adminService.banUser(userUuid,true);
            msg="UnBan user successfully...";
        }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }

    }
        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime),HttpStatus.OK);
    }




    @GetMapping("get-user-info")
    @Operation(summary = "get user info")
    ResponseEntity<?> getUserInfoByUuid(@RequestParam @Valid @NotBlank(message = "user uuid cannot be blank") String uuid) {
        String msg= "";
        UserInfoPayload user = new UserInfoPayload();
        try
        {
            user = userService.getUserInfo(uuid);
            if(user==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this uuid"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

                msg="User info Fetching successfully";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Fail to fetch data ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new UserInfoResponse(202,msg,requestTime,user),HttpStatus.OK);

    }




    @GetMapping("get-all-users")
    @Operation(summary = "get all user")
    ResponseEntity<?> getAllUsers(@RequestParam(defaultValue = "10") int limit,
                                  @RequestParam(defaultValue = "1") int page)  {
        String msg= "";
        List<GetAllUserPayload> storeAllUser= new ArrayList<>();
        Paging paging = new Paging ( );
        paging.setPage ( page );
        int offset = (page - 1) * limit;
        paging.setLimit ( limit );
        try
        {
            storeAllUser=adminService.getAllUser(limit,offset);
            System.out.println("here is the find user"+storeAllUser);
        }
        catch (Exception e)
        {
          errors.add(new ErrorPayload("Error","\" Fetching data exception: \" + e.getMessage ( ) "));
          return new ResponseEntity<>(new ErrorResponse(404,"Error found while fetching the users..",requestTime,errors),HttpStatus.BAD_REQUEST);
        }


        return new ResponseEntity<>(new GetAllUserResponse(202,"User was fetch  successfully",requestTime,storeAllUser),HttpStatus.FOUND);
    }

    @GetMapping("search-user-by-name")
    @Operation(summary = "search user by name")
    ResponseEntity<?> searchUserByName(@RequestParam @NotBlank(message = "user name cannot be blank") String name) {
        List<SearchUserPayload> userResult= new ArrayList<>();
        try {
            userResult= storyService.searchUserByName(name);
            if(userResult==null)
            {
                errors.add(new ErrorPayload("User","Can not find any user with this email."));
                return new ResponseEntity<>(new ErrorResponse(400,"User not found..",requestTime,errors),HttpStatus.NOT_FOUND);
            }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        String msg= "User fetching successfully";
        return new ResponseEntity<>(new SearchUserByNameResponse( 201,msg,requestTime,userResult), HttpStatus.OK);
    }


    @DeleteMapping("delete-story-request")
    @Operation(summary = "delete story request")
    ResponseEntity<?> deleteStoryRequest(@RequestParam @Valid @NotBlank(message = "StoryTypeUuid can not be blank") String storyTypeUuid) {
        String msg= "";
        try
        {
            int result =adminService. deleteStoryRequest(storyTypeUuid);
            System.out.println(result);
            if(result==0){
                errors.add(new ErrorPayload("Story Type","Can not find any storyType with this email !!!"));
                return new ResponseEntity<>(new ErrorResponse(403,"Story Type",requestTime,errors),HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e)
        {
            errors.add(new ErrorPayload("SQL Exception","Error while try to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(403,"Errors",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(202,"Story Type was delete Successfully ",requestTime),HttpStatus.FOUND);
    }




    @GetMapping("get-all-story-request")
    @Operation(summary = "get all story request")
    ResponseEntity<?> getAllStoryRequest() {
        List<GetAllStoryRequestPayload> allRequest = new ArrayList<>();
        String msg= "";
        try {
            allRequest= adminService.getAllStoryRequest();
            System.out.println(allRequest);
            if(allRequest==null)
            {
               msg="There are no story Request ...";
            }else {
                msg="Story Type fetching successfully";
            }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new GetAllStoryRequestResponse( 201,msg,requestTime,allRequest), HttpStatus.OK);
    }

    @GetMapping("/get-story-by-uuid")
    @Operation(summary = "get story by uuid")
    ResponseEntity<?> getStoryByUuid(@RequestParam @Valid String storyUuid)
    {
        errors.removeAll(errors);
        boolean check=false;
        GetStroyByUuidPayload stroyByUuidPayload = new GetStroyByUuidPayload();
        try
        {
            stroyByUuidPayload= storyService.getStoryByUuid(storyUuid);

            if(stroyByUuidPayload==null)
            {
                errors.add(new ErrorPayload("Story","cannot find any story with this uuid"+storyUuid));
                return new ResponseEntity<>(new ErrorResponse(404,"Story not found",requestTime,errors),HttpStatus.NOT_FOUND);
            }else {
                int oldVeiw= storyService.getVeiwCount(storyUuid);

                storyService.incressView(storyUuid,oldVeiw+1);
            }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new GetStroyByUuidRespone(200,"Fetch Story Successfully",requestTime,stroyByUuidPayload),HttpStatus.OK);
    }

    @GetMapping("/get-all-story-by-uuid")
    @Operation(summary = "get all story by story uuid")
    ResponseEntity<?> getAllStoryByUserId(@RequestParam String uuid)
    {
        //clear global error  to avoid repeat print old error
        errors.removeAll(errors);
        String msg="";
        List<GetAllPostByUserIDPayload> getAllStroyByUserId = new ArrayList<>();
        try
        {
            getAllStroyByUserId= storyService.getStoryByUserId(uuid);
//                System.out.println("Here is al "+ getAllStroyByUserId);
            if(getAllStroyByUserId.isEmpty())
            {
                msg="No Story !!!";
                return new ResponseEntity<>(new GetAllPostByUserIDRespone(200,msg,requestTime,getAllStroyByUserId),HttpStatus.OK);
            }
            else {

                msg="Fetch Story Successfully";
            }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new GetAllPostByUserIDRespone(200,msg,requestTime,getAllStroyByUserId),HttpStatus.OK);
    }





}
