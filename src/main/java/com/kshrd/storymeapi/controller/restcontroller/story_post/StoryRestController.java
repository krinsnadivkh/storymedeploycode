package com.kshrd.storymeapi.controller.restcontroller.story_post;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.payload.MemorizeStoryPayload;
import com.kshrd.storymeapi.dto.request.sotry_post.UpdateStoryPrivacyRequest;
import com.kshrd.storymeapi.dto.request.story.*;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.story.*;
import com.kshrd.storymeapi.dto.response.users.SearchUserByNameResponse;
import com.kshrd.storymeapi.dto.response.users.bestUserResponse;
import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import com.kshrd.storymeapi.model.story.RequestStory;
import com.kshrd.storymeapi.model.story.Story;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.service.story.StoryService;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.customfuntion.CustomFuntion;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController

@RequiredArgsConstructor
@RequestMapping("/api/v1/story-post")
public class StoryRestController {
    Date requestTime = new Date();
    @Autowired
    StoryService storyService;
    @Autowired
    UserService userService;
    List<ErrorPayload> errors = new ArrayList<>();
    CustomFuntion customFuntion = new CustomFuntion();
    @PostMapping("/create-story")
    @Operation(summary = "Create Story")
    ResponseEntity<?> crateJourney(@RequestBody @Valid StoryRequest storyRequest) {
        Story story = new Story();
        Users checkUserIfMatch = new Users();
        int storyTypeID = 0;
        int storyId = 0;
        GetStroyByUuidPayload res = new GetStroyByUuidPayload();
        try {
            checkUserIfMatch = userService.finduserByUuid(storyRequest.getUserUuid());
            System.out.println("Here is cheuser"+checkUserIfMatch);
                    if(storyRequest.getTitle().isEmpty()||storyRequest.getStoryType().isEmpty()||storyRequest.getEmotionID()==0||storyRequest.getUserUuid().isEmpty())
                    {
                        errors.removeAll(errors);
                        errors.add(new ErrorPayload("Input Feil","Sorry All feil are required "));
                    return new ResponseEntity<>(new ErrorResponse(406,"Sorry there are problem creating post",requestTime,errors),HttpStatus.BAD_REQUEST);
                    }
                    else if(!storyRequest.getTitle().isEmpty()||!storyRequest.getStoryType().isEmpty()||storyRequest.getEmotionID()!=0||!storyRequest.getUserUuid().isEmpty())
                    {
                        storyTypeID=  storyService.getStoryTypeIDByName(storyRequest.getStoryType());
                        story.setStoryTypeID(storyTypeID);
                        System.out.println("moti"+storyTypeID);
                        System.out.println("sdoti"+checkUserIfMatch);
                         if(storyTypeID==0)
                    {
                        errors.add(new ErrorPayload("Story Type","Sorry can not find story type with this Type= "+storyRequest.getStoryType() ));
                        return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating post",requestTime,errors),HttpStatus.BAD_REQUEST);

                    }
                    else if(checkUserIfMatch==null){
                        errors.add(new ErrorPayload("User","Sorry can not find user with this uuid= "+storyRequest.getUserUuid()));
                        return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating post",requestTime,errors),HttpStatus.BAD_REQUEST);
                    }
                    else
                    {
                        System.out.println(storyRequest);
                        story.setTitle(storyRequest.getTitle());
                        story.setEmotionID(storyRequest.getEmotionID());
//                        story.setUuid(UUID.fromString(storyRequest.getUserUuid()));

                        story.setUuid(storyRequest.getUserUuid());
                        story.setPublic(storyRequest.isPublis());
                        story.setLikeCount(0);
                        story.setCommentCount(0);
                        story.setVeiwCount(0);
                        story.setOwnerId(storyRequest.getUserUuid());
                        story.setUserId(userService.getUserIdByUuid(storyRequest.getUserUuid()));

                        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = new Date();

                        story.setCreateDate(formatter.format(date));
                        System.out.println("Here is the user id "+ story.getUserId());
                        System.out.println("Her is "+ story.isPublic());
                        storyId= storyService.crateStory(story);

                    res = storyService.getStoryCreateResponse(storyId);
                        System.out.println("add"+res);

                    }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Create Story","There are some eror while try to creating post"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating post",requestTime,errors),HttpStatus.BAD_REQUEST);
        }

//        System.out.println("Here is id"+ storyId);
        return new ResponseEntity<>(new createStoryRespone(201,"Story Post successfully... ",requestTime,res),HttpStatus.CREATED);
    }



    @PostMapping("/add-paragraphs")
    @Operation(summary = "Create Paragraphs")
    ResponseEntity<?> crateParagraphs( @RequestBody  @Valid List<ParagraphsRequest> paragraphs,Errors error, @RequestParam int StoryId ) {
        Paragraphs pr = new Paragraphs();
        System.out.println(pr);
        try{
            if (storyService.findStroyById(StoryId)== null){
                System.out.println("Story not found");
            }
            else {
                for (ParagraphsRequest p:paragraphs) {
                    pr.setParagraph(p.getParagraphs());
                    pr.setImage(p.getImage());
                    pr.setStoryId(StoryId);
                    int a =storyService.crateParagraphs(pr);
                    System.out.println("here is the result "+ a);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Paragraphs","There are some eror while try to adding paragraphs"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating paragrahs",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Story Post successfully... ",requestTime),HttpStatus.CREATED);
    }

    @GetMapping("/getAllByUseId")
    @Operation(summary = "get all story by provide user uuid")
    ResponseEntity<?> getAllStoryByUserId(@RequestParam String uuid)
    {
        //clear global error  to avoid repeat print old error
            errors.removeAll(errors);
            boolean check=false;
            String msg="";
            List<GetAllPostByUserIDPayload> getAllStroyByUserId = new ArrayList<>();
            try
            {
                getAllStroyByUserId= storyService.getStoryByUserId(uuid);
//                System.out.println("Here is al "+ getAllStroyByUserId);
                if(getAllStroyByUserId.isEmpty())
                {
                    msg="No Story found !!!";
                    return new ResponseEntity<>(new GetAllPostByUserIDRespone(200,msg,requestTime,getAllStroyByUserId),HttpStatus.OK);
                }
                else {
                    msg="Fetch Story Successfully";
                }

            }catch (NotFoundException e)
            {
                errors.add(new ErrorPayload("Error",e.getMessage()));
                return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
        return new ResponseEntity<>(new GetAllPostByUserIDRespone(200,msg,requestTime,getAllStroyByUserId),HttpStatus.OK);
    }


//    @PostMapping("/add-comment")
//    @Operation(summary = "add comment")
//    ResponseEntity<?> addComment(@RequestBody @Valid CommentRequest commentRequest) {
//        errors.removeAll(errors);
//        Comments cm= new Comments();
//         cm.setContent(commentRequest.getContent());
//         cm.setStoryId(commentRequest.getStoryId());
////         cm.setUserUuid(commentRequest.getUserUuid());
//         int userId = userService.getUserIdByUuid(commentRequest.getUserUuid());
//         cm.setUserId(userId);
//
////         cm.setUserId(commentRequest.getUserId());
////        System.out.println(cm);
//        try{
//            int commentId =storyService.crateComment(cm);
////            System.out.println("Hi a"+ a);
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("Comment","There are some eror while try to adding Comments"));
//            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem adding Commments",requestTime,errors),HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(new SuccessResponse(201,"Commet add successfully... ",requestTime),HttpStatus.CREATED);
//    }


    @PostMapping("/add-comment")
    @Operation(summary = "add comment")
    ResponseEntity<?> addComment(@RequestBody @Valid CommentRequest commentRequest) {
        errors.removeAll(errors);
        Comments cm= new Comments();
        cm.setContent(commentRequest.getContent());
        cm.setStoryId(commentRequest.getStoryId());
//         cm.setUserUuid(commentRequest.getUserUuid());
        int userId = userService.getUserIdByUuid(commentRequest.getUserUuid());
        cm.setUserId(userId);
        CommentPayload commentPayload = new CommentPayload();
//         cm.setUserId(commentRequest.getUserId());
//        System.out.println(cm);
        try{
            int commentId =storyService.crateComment(cm);
            System.out.println("this is comment Id"+commentId);
            commentPayload=storyService.getCommentById(commentId);
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Comment","There are some eror while try to adding Comments"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem adding Commments",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new CreateCommentResponse(201,"Comment add successfully... ",requestTime,commentPayload),HttpStatus.CREATED);
    }





    @DeleteMapping("/delete-story")
    @Operation(summary = "delete story  by story uuid")
    ResponseEntity<?> deleteStroy(@RequestParam @NotNull String storyId) {
        errors.removeAll(errors);

        try{
            int result = storyService.deleteStroy(storyId);
            if(result==0)
            {
//                System.out.println("can not find any post with uuid = "+storyId);
                errors.add(new ErrorPayload("post","Cannot find any post with uuid = "+storyId));
                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Comment","There are some eror while try to adding Comments"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to delete the story",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Delete stroy successfully... ",requestTime),HttpStatus.CREATED);
    }





    @PatchMapping("/update-story")
    @Operation(summary = "update story by post uuid")
    ResponseEntity<?> updateStroy(@RequestBody @Valid UpdateStoryRequest  updateStoryRequest) {
        errors.removeAll(errors);
        UpdateStoryPayload up = new UpdateStoryPayload();
        UpdateResponse upRespone = new UpdateResponse();
        int storyTypeID;
        try{
            storyTypeID=  storyService.getStoryTypeIDByName(updateStoryRequest.getStoryType());
            up.setStorytypeId(storyTypeID);
//            System.out.println("Here is the typeId"+storyTypeID);
            up.setStorytypeId(storyTypeID);
            up.setPostUuid(updateStoryRequest.getPostUuid());
            up.setTitle(updateStoryRequest.getTitle());
            up.setPublis(updateStoryRequest.isPublis());
            up.setEmotionID(updateStoryRequest.getEmotionID());
//            System.out.println(up);
            int a= storyService.updateStroyByUuid(up);
//            System.out.println("jhere is a "+ a);
            if (a!=0)
            {

//                System.out.println(updateStoryRequest.getPostUuid());
                System.out.println(""+storyService.getStoryByStroyUuid(updateStoryRequest.getPostUuid()));
               upRespone =storyService.getStoryByStroyUuid(updateStoryRequest.getPostUuid());
            }
//            System.out.println(a);
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Comment","There are some eror while try to update "));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to update the story",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
//        return new ResponseEntity<>(new SuccessResponse(201,"Update done",requestTime),HttpStatus.CREATED);
        return new ResponseEntity<>(new UpdateStoryRespone(202,"Update Story Done ",requestTime,upRespone),HttpStatus.FOUND);
    }

//    @PatchMapping("/update-paragraphs")
//    @Operation(summary = "update paragraphs by paragraphs id")
//    ResponseEntity<?> updateParagraphs(@RequestBody @Valid List<ParagraphsUpdateRequest> paragraphs) {
//        errors.removeAll(errors);
//        ParagraphsUpdateRequest pa = new ParagraphsUpdateRequest();
//       int result=0;
//       boolean checkFound=false;
////        System.out.println("Here is paragraph"+paragraphs);
//        try{
//            for (ParagraphsUpdateRequest p:paragraphs ) {
//                pa.setParagraphs(p.getParagraphs());
//                pa.setImage(p.getImage());
//                pa.setUuid(p.getUuid());
//              Paragraphs pCheck=  storyService.findParagraphByUUid(p.getUuid());
//                if(pCheck==null)
//                {
//                  result++;
//                }
//                else {
//                    checkFound=true;
//                }
//                if (result!=0&&checkFound==false)
//                {
//                    errors.add(new ErrorPayload("Paragraphs","Cannot find any Paragraphs with this uuid..."));
//                    return new ResponseEntity<>(new ErrorResponse(400,"Cannot update this is paragraphs...",requestTime,errors),HttpStatus.BAD_REQUEST);
//                }else
//                {
//                    storyService.updateParagraphsById(pa);
//                }
//
//            }
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("Paragraphs","There are some problem while try to update paragraphs"));
//            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to update the story",requestTime,errors),HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(new SuccessResponse(202,"Paragraphs was Update successfully",requestTime),HttpStatus.OK);
//    }
//

    @PatchMapping("/update-paragraphs")
    @Operation(summary = "update paragraphs by paragraphs id")
    ResponseEntity<?> updateParagraphs(@RequestBody @Valid List<ParagraphsUpdateRequest> paragraphs) {
        errors.removeAll(errors);
        ParagraphsUpdateRequest pa = new ParagraphsUpdateRequest();
        int result=0;
        boolean checkFound=false;
//        System.out.println("Here is paragraph"+paragraphs);
        try{
            for (ParagraphsUpdateRequest p:paragraphs ) {
                pa.setParagraphs(p.getParagraphs());
                pa.setImage(p.getImage());
//                pa.setUuid(p.getUuid());
                pa.setId(p.getId());
//                Paragraphs pCheck=  storyService.findParagraphByUUid(p.getUuid());
                Paragraphs pCheck=  storyService.findParagraphById(p.getId());

                if(pCheck==null)
                {
                    result++;
                }
                else {
                    checkFound=true;
                }
                if (result!=0&&checkFound==false)
                {
                    errors.add(new ErrorPayload("Paragraphs","Cannot find any Paragraphs with this uuid..."));
                    return new ResponseEntity<>(new ErrorResponse(400,"Cannot update this is paragraphs...",requestTime,errors),HttpStatus.BAD_REQUEST);
                }else
                {
                    storyService.updateParagraphsByMyId(pa);
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Paragraphs","There are some problem while try to update paragraphs"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to update the story",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(202,"Paragraphs was Update successfully",requestTime),HttpStatus.OK);
    }












//    @PutMapping("/updatePrivacyBy-storyID")
//    @Operation(summary = "update privacy")
//    ResponseEntity<?> updateStoryById(@RequestParam String uuid, @RequestParam  boolean privacy) {
//        errors.removeAll(errors);
////        System.out.println("uuid ="+uuid);
////        System.out.println("privacy"+privacy);
//            try{
//
//               Story stCheck= storyService.findStroyByUuid(uuid);
//                if(stCheck!=null)
//                {
//                    int result=   storyService.updatePrivacyByUuid(uuid,privacy);
//                }else
//                {
//                    errors.add(new ErrorPayload("story","Cannot find any story with this  uuid"));
//                    return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//                }
//
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//                errors.add(new ErrorPayload("post","Cannot find any story with uuid ="));
//                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//            }
//
//        return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully... ",requestTime),HttpStatus.CREATED);
//    }


//    sna
@PutMapping("/updatePrivacyBy-id")
@Operation(summary = "update privacy")
ResponseEntity<?> updateStoryById(@RequestBody UpdateStoryPrivacyRequest up) {
    errors.removeAll(errors);
//        System.out.println("uuid ="+uuid);
//        System.out.println("privacy"+privacy);
    try{

        Story stCheck= storyService.findStroyByUuid(up.getUuid());
        if(stCheck!=null)
        {
            int result=   storyService.updatePrivacyByUuid(up.getUuid(),up.isPublic());
        }else
        {
            errors.add(new ErrorPayload("story","Cannot find any story with this  uuid"));
            return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
        }

    }
    catch (Exception e)
    {
        e.printStackTrace();
        errors.add(new ErrorPayload("post","Cannot find any story with uuid ="));
        return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully... ",requestTime),HttpStatus.CREATED);
}




    @PostMapping("/like-story")
    @Operation(summary = "React story by check condition")
    ResponseEntity<?> reactStory(@RequestParam int storyId,@RequestParam int userId ) {
      String msg= "";
        if(storyService.findStroyById (storyId) == null) {
            errors.add(new ErrorPayload("story", "Cannot find any story with this is Id"));
            return new ResponseEntity<>(new ErrorResponse(400, "Story Not Found", requestTime, errors), HttpStatus.NOT_FOUND);
        }
        if(storyService.isLikePost(storyId,userId)>0)
        {
            storyService.unLikePost(storyId,userId);
             msg="Unlike story Id ="+storyId+"Succcessfully...";
        }
        else {
            System.out.println("this is like"+  storyService.likeStoryPost(storyId,userId));
            msg="Like story Id ="+storyId+"Succcessfully...";
        }
        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime),HttpStatus.OK);
    }

    @GetMapping("/get-story-by-uuid")
    @Operation(summary = "Read More get story by uuid")
    ResponseEntity<?> getStoryByUuid(@RequestParam String storyUuid)
    {
        errors.removeAll(errors);
        boolean check=false;
        GetStroyByUuidPayload stroyByUuidPayload = new GetStroyByUuidPayload();
        try
        {
            stroyByUuidPayload= storyService.getStoryByUuid(storyUuid);

           if(stroyByUuidPayload==null)
            {
                errors.add(new ErrorPayload("Story","cannot find any story with this uuid"+storyUuid));
                return new ResponseEntity<>(new ErrorResponse(404,"Story not found",requestTime,errors),HttpStatus.NOT_FOUND);
            }else {
               int oldVeiw= storyService.getVeiwCount(storyUuid);

               storyService.incressView(storyUuid,oldVeiw+1);
           }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new GetStroyByUuidRespone(200,"Fetch Story Successfully",requestTime,stroyByUuidPayload),HttpStatus.OK);
    }



    @PostMapping("/request-story-type")
    @Operation(summary = "Request Story Type")
    ResponseEntity<?> requestStoryType(@RequestParam String storyType,@RequestParam int UserId)
    {
        errors.removeAll(errors);
        RequestStory rs = new RequestStory();
        try
        {
            rs.setStoryType(storyType);
            rs.setAllow(false);
            rs.setUserId(UserId);
            int result= storyService.requestStoryType(rs);
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to Request Story Type. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new SuccessResponse(200,"Story type have been requst Successfully",requestTime),HttpStatus.OK);
    }





    @GetMapping("/get-recentStory")
    @Operation(summary = "get recent Story")
    ResponseEntity<?> getRecentStory()
    {
        //clear global error  to avoid repeat print old error
        errors.removeAll(errors);
        boolean check=false;
  List< RecentpostPayload>  rp = new ArrayList<>();
        try
        {
            rp= storyService.getRecentStory();
//            System.out.println(rp);

        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new RecentPostResponse(200,"Fetch Story Successfully",requestTime,rp),HttpStatus.OK);
    }


    @GetMapping("/search-story-by-title")
    @Operation(summary = "Search Story By Title")
    ResponseEntity<?> searchStoryByTitle(@RequestParam String storyTitle)
    {
        errors.removeAll(errors);
        List< SearchStoryPostPayload>  storyResult = new ArrayList<>();
        try
        {
            storyResult=storyService.searchStoryIbByTitle(storyTitle);
            if(storyResult.isEmpty())
            {
                return new ResponseEntity<>(new SearchByTitleOnlyResponse(404,"Opp ! There are no post with this title !!!",requestTime,storyResult),HttpStatus.NOT_FOUND);
            }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new SearchByTitleOnlyResponse(200,"Fetch Story Successfully",requestTime,storyResult),HttpStatus.OK);
  }

    @GetMapping("/search-story-by-type")
    @Operation(summary = "Search Story By type")
    ResponseEntity<?> searchStoryByType(@RequestParam String storyType)
    {
        errors.removeAll(errors);
        List< SearchStoryPostPayload>  storyResult = new ArrayList<>();
        try
        {
          int StoryTypeId=  storyService.getStoryTypeIDByName(storyType);
//            System.out.println("I d "+ StoryTypeId);
            storyResult=storyService.searchStoryByType(StoryTypeId);
            if(storyResult.isEmpty())
            {
                return new ResponseEntity<>(new SearchByTitleOnlyResponse(404,"Opp ! There are no post with this title !!!",requestTime,storyResult),HttpStatus.NOT_FOUND);
            }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new SearchByTitleOnlyResponse(200,"Fetch Story Successfully",requestTime,storyResult),HttpStatus.OK);
    }




    @GetMapping("search-user-by-name")
    @Operation(summary = "search user by name")
    ResponseEntity<?> searchUserByName(@RequestParam @NotNull String name) {
    List<SearchUserPayload> userResult= new ArrayList<>();
        try {
            userResult= storyService.searchUserByName(name);
            if(userResult==null)
            {
                errors.add(new ErrorPayload("User","Can not find any user with this email."));
                return new ResponseEntity<>(new ErrorResponse(400,"User not found..",requestTime,errors),HttpStatus.NOT_FOUND);
            }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        String msg= "User fetching successfully";
        return new ResponseEntity<>(new SearchUserByNameResponse( 201,msg,requestTime,userResult), HttpStatus.OK);
    }




    @GetMapping("get-all-popular-story")
    @Operation(summary = "get all popular story")
    ResponseEntity<?> getAllPopularStory() {
        errors.removeAll(errors);
        boolean check=false;
        List< RecentpostPayload>  rp = new ArrayList<>();
        try
        {
            rp= storyService.getPopularStory();
            if(rp==null )
            {
                errors.add(new ErrorPayload("Story","There are no popular Story To show"));
                return new ResponseEntity<>(new ErrorResponse(404,"No Popular Story!!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
        }catch (NotFoundException e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new RecentPostResponse(200,"Fetch Story Successfully",requestTime,rp),HttpStatus.OK);
    }

    @GetMapping("get-one-year-memorize-story")
    @Operation(summary = "get memorize story by user token")
    ResponseEntity<?> getMemorizeStory(@RequestParam String userUuid) {
        LocalDateTime currentVisit = LocalDateTime.now();
        LocalDate start = LocalDate.now();
//        System.out.println("Here is the vistie " + currentVisit);
        errors.removeAll(errors);
        boolean check = false;
        List<RecentpostPayload> rp = new ArrayList<>();
        List<MemorizeStoryPayload> memorizeOneYears;
        try {
            LocalDate currentDate = LocalDate.now();
            LocalDate createDate = LocalDate.of(currentDate.getYear() - 1, currentDate.getMonth(), currentDate.getDayOfMonth());
            String myDate= String.valueOf(createDate);
//            System.out.println("mydate"+myDate);
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-uuuu");
            String memorizeBy = createDate.format(formatters);
//            System.out.println("mydate time"+memorizeBy);

            memorizeOneYears = storyService.getMemorizeOneYears(memorizeBy,userUuid);
        } catch (NotFoundException e) {
            errors.add(new ErrorPayload("Error", e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404, "Sorry fail to query Data. There are some error while processing !!!", requestTime, errors), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new MemorizeStoryResponse(200, "Fetch Story Successfully", requestTime, memorizeOneYears), HttpStatus.OK);
    }


//    v 2
//    get all following story post
@GetMapping("/get-followingStory")
@Operation(summary = "get following Story")
ResponseEntity<?> getFollowingStory()
{
    //clear global error  to avoid repeat print old error
    System.out.println();
    errors.removeAll(errors);
    boolean check=false;
    List< GetAllPostByUserIDPayload>  followingPost = new ArrayList<>();

    try
    {
        CustomFuntion myCus= new CustomFuntion();

        int userID= userService.getUserIdByEmail(myCus.getCurrentUser());
//        System.out.println(userID);
        List<Integer> getAllUserFollowing = storyService.getAllFollowingIDByUserId(userID);
        for (int i:getAllUserFollowing) {

            followingPost.add(storyService.getAllStoryByUserId(i));
        }
//
//        System.out.println("sna"+followingPost);
//        System.out.println("here is the fpll"+getAllUserFollowing);
    }catch (NotFoundException e)
    {
        errors.add(new ErrorPayload("Error",e.getMessage()));
        return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(new GetAllPostByUserIDRespone(200,"Fetch Story Successfully",requestTime,followingPost),HttpStatus.OK);
}

    @GetMapping("/getUserOfTheMonth")
    @Operation(summary = "get User Of The Month")
    ResponseEntity<?> getUserOfTheMonth() {
    String start = null;
    String end = null;
    List<UserInfoPayload> userofTheMonth = new ArrayList<>();
        ArrayList<String> daysList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        for (int i = 1; i < maxDay + 1; i++) {
            cal.set(Calendar.DAY_OF_MONTH, i);
            String date = df.format(cal.getTime());
            daysList.add(date);
            System.out.println("this is the mmourn "+date);
        }
        if(!daysList.isEmpty())
        {
            start= daysList.get(0);
            end = daysList.get(daysList.size()-1);
            System.out.println("This is the start date="+ start+"and end is"+ end);
        }
        List<GetAllPostByUserIDPayload> listStroy= storyService.getUserOfTheMonth(start,end);
        System.out.println("this is the shor"+ listStroy);

        for (GetAllPostByUserIDPayload user:listStroy
             ) {
            System.out.println("Here is user Id"+ user.getUsers().getUser_uuid());
            if(user.getVeiwCount()!=0)
            {
                userofTheMonth.add( userService.getUserInfo(user.getUsers().getUser_uuid()));
            }
        }
        LinkedHashSet<UserInfoPayload> hashSet = new LinkedHashSet<>(userofTheMonth);
        ArrayList<UserInfoPayload> userNotDub= new ArrayList<>(hashSet);
        for (UserInfoPayload u:userNotDub
        ) {
            if(userService.isBaseUser(u.getId(),1)==0) {
                storyService.insertBestUser(u.getId(), 1);
            }
        }
        return new ResponseEntity<>(new bestUserResponse(200,"User of the Month  Fetch Story Successfully",requestTime, userNotDub),HttpStatus.OK);
    }




    @GetMapping("/getUserOfTheWeek")
    @Operation(summary = "get User Of The Week")
    ResponseEntity<?> getUserOfTheWeek() {
        String start=null;
        String end=null;
        List<GetAllPostByUserIDPayload> allStoriesList = new ArrayList<>();
        List<UserInfoPayload> userofTheWeek = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String[] days = new String[7];
        int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++)
        {
            days[i] = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }
        start = days[0];
        end = days[6];
        List<GetAllPostByUserIDPayload> listStroy= storyService.getUserOfTheMonth(start,end);
        System.out.println("tnis start date"+ start);
        System.out.println("this end date"+ end);
        for (GetAllPostByUserIDPayload user:listStroy) {
            System.out.println("Here is user Id"+ user.getTitle());
            if(user.getVeiwCount()!=0)
            {
                userofTheWeek .add( userService.getUserInfo(user.getUsers().getUser_uuid()));
            }
        }
        System.out.println("here is the uer of the week"+userofTheWeek);

        LinkedHashSet<UserInfoPayload> hashSet = new LinkedHashSet<>(userofTheWeek );
        ArrayList<UserInfoPayload> userNotDub= new ArrayList<>(hashSet);
        for (UserInfoPayload u:userNotDub
             ) {
            if(userService.isBaseUser(u.getId(),2)==0)
            {
                storyService.insertBestUser(u.getId(),2);
            }
        }
        return new ResponseEntity<>(new bestUserResponse(200,"User of the WeekFetch Story Successfully",requestTime, userNotDub),HttpStatus.OK);

  }

    @GetMapping("/getAllStoryType")
    @Operation(summary = "get all story type")
    ResponseEntity<?> getAllStoryType() {
        List<String> allStoryType= new ArrayList<>();
            try{

                allStoryType= storyService.getAllStoryType();

              }
            catch (Exception e)
            {
                errors.add(new ErrorPayload("Error",e.getMessage()));
                return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }

        return new ResponseEntity<>(new AllStoryTypeResponse(200,"Story type was fetch Story Successfully",requestTime, allStoryType),HttpStatus.OK);

    }



    @GetMapping("/getAllEmotion")
    @Operation(summary = "get all emotion")
    ResponseEntity<?> getAllEmotion() {
        List<EmotionPayload> allEmotion= new ArrayList<>();
        try{

            allEmotion= storyService.getAllEmotion();

        }
        catch (Exception e)
        {
            errors.add(new ErrorPayload("Error",e.getMessage()));
            return new ResponseEntity<>(new ErrorResponse(404,"Sorry fail to query Data. There are some error while processing !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new AllEmotionResponse(200,"Story type was fetch Story Successfully",requestTime, allEmotion),HttpStatus.OK);

    }



//new modify
@GetMapping("/isLikeStory")
@Operation(summary = "isLikeStory")
ResponseEntity<?> islikeStory(@RequestParam int storyId) {
    String msg= "";
    String curentUserEmail= "";
    boolean check=false;
    int ownerId = 0;
    if(storyService.findStroyById (storyId) == null) {
        errors.add(new ErrorPayload("story", "Cannot find any story with this is Id"));
        return new ResponseEntity<>(new ErrorResponse(400, "Story Not Found", requestTime, errors), HttpStatus.NOT_FOUND);
    }

    curentUserEmail=  customFuntion.getCurrentUser();
    if(curentUserEmail=="anonymousUser")
    {
        errors.add(new ErrorPayload("Login Required","You have  not login you can not access this resource !!!"));
        return new ResponseEntity<>(new ErrorResponse(404,"Not allow ",requestTime,errors),HttpStatus.BAD_REQUEST);
    }

    else {
        ownerId=  userService.getUserIdByEmail(curentUserEmail);
        System.out.println("Here is the ower id"+ownerId);
        if(ownerId==0)
        {
            errors.add(new ErrorPayload("Users", "Sorry cannot find any user with this email !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"User not found !!! ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        else
        {
            if(storyService.isLikePost(storyId,ownerId)>0)
            {
                msg="Story already like ...";
                System.out.println("like");
                check=true;
            }
            else {
                msg="Story not yet like...";
                check=false;
            }
        }
    }














    return new ResponseEntity<>(new IsLikeResponse(201,msg,requestTime,check),HttpStatus.OK);
}



//this is the comment


}
