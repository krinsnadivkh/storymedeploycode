package com.kshrd.storymeapi.controller.restcontroller.todo_list;

import com.kshrd.storymeapi.dto.payload.ErrorPayload;
import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;
import com.kshrd.storymeapi.dto.request.sotry_post.UpdateStoryPrivacyRequest;
import com.kshrd.storymeapi.dto.request.todo_list.CreateTodoRequest;
import com.kshrd.storymeapi.dto.request.todo_list.TodoListRequest;
import com.kshrd.storymeapi.dto.request.todo_list.UpdateCommpleteStatusRequest;
import com.kshrd.storymeapi.dto.request.todo_list.UpdateTodoListRequest;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.journey.GetAllJourneyResponse;
import com.kshrd.storymeapi.dto.response.todolist.CreateTodoListResponse;
import com.kshrd.storymeapi.dto.response.todolist.CreateTodoResponse;
import com.kshrd.storymeapi.dto.response.todolist.GetAllTodoListByUserUuidResponse;
import com.kshrd.storymeapi.dto.response.todolist.TodoMyResponse;
import com.kshrd.storymeapi.dto.response.users.UserLoginResponse;
import com.kshrd.storymeapi.model.story.Story;
import com.kshrd.storymeapi.model.todo_list.Todo;
import com.kshrd.storymeapi.model.todo_list.TodoList;
import com.kshrd.storymeapi.repository.todo_list.TodoListRepository;
import com.kshrd.storymeapi.service.todo_list.TodoListService;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.paging.Paging;
import io.swagger.v3.oas.annotations.Operation;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/todo-list")
public class TodoListRestController {

    @Autowired
    TodoListService todoListService;
    @Autowired
    UserService userService;
    Date requestTime = new Date();
    List<ErrorPayload> errors = new ArrayList<>();
    @PostMapping("create-todolist")
    @Operation(summary = "create to do-list")
    ResponseEntity<?> createTodoList(@RequestBody @Valid TodoListRequest todoListRequest) {
        String msg= "";
        TodoList td = new TodoList();
        GetAllTodoListPayload getAllTodoListPayload = new GetAllTodoListPayload();
        int todoListId;
        int userId ;
        if(userService.finduserByUuid(todoListRequest.getUserUuid())==null){
            errors.add(new ErrorPayload("User","Cannot find any user with this uuid..."));
            return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        else {
            try {
                td.setCreateDate(todoListRequest.getCreateDate());
                td.setPublish(todoListRequest.isPublish());
                td.setTodoDate(todoListRequest.getTodoDate());
                td.setUserId(userService.getUserIdByUuid(todoListRequest.getUserUuid()));
                td.setPublish(todoListRequest.isPublish());
              todoListId = todoListService.createTodoList(td);
              if(todoListId!=0)
              {
                  getAllTodoListPayload =todoListService.getTodolistById(todoListId);
              }
                msg="TodoList Create Successfully";
            }catch (Exception e)
            {
                errors.add(new ErrorPayload("Error","There are error while create to do-list problem wiht database !!!"));
                return new ResponseEntity<>(new ErrorResponse(404,"Cannot create To do-list",requestTime,errors),HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(new CreateTodoListResponse(201,msg,requestTime,getAllTodoListPayload),HttpStatus.OK);
    }

    @PostMapping("create-todo")
    @Operation(summary = "create todo")
    ResponseEntity<?> createTodo(@RequestBody @Valid List<CreateTodoRequest> todo,@RequestParam int todoListId) {
        String msg= "";
        String todoTime;
        Todo td = new Todo();
        List<Todo> myTodo = new ArrayList<>();
        errors.removeAll( errors);
        try {

           if( todoListService.findTodoListById(todoListId)==null)
           {
               errors.add(new ErrorPayload("TodoList","Cannot find any todoList with this id ="+todoListId +"!!!"));
               return new ResponseEntity<>(new ErrorResponse(404,"To do-list not found",requestTime,errors),HttpStatus.BAD_REQUEST);
           }else {
               for (CreateTodoRequest t: todo) {
                   td.setAction(t.getAction());
                   todoTime=t.getTime();
                   td.setTodoTime(todoTime);
                   td.setComplete(false);
                   td.setTodolistId(todoListId);
                   int a = todoListService.createTodo(td);
               }
               myTodo= todoListService.getTodoByTodoListId(todoListId);



           }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("Error","There are error while create to do-list problem with database !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Cannot create To do",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new TodoMyResponse(201,"To do create Successfully....",requestTime,myTodo),HttpStatus.OK);
    }



    @PatchMapping("update-todolist")
    @Operation(summary = "Update Todo List")
    ResponseEntity<?> updateTodoList(@RequestBody @Valid UpdateTodoListRequest updateTodoListRequest) {
        String msg= "";
        TodoList td= new TodoList();
        errors.removeAll(errors);
        try{

            System.out.println("ed"+ todoListService.findTodoListByUuid(updateTodoListRequest.getTodoListUuid()));

            if(todoListService.findTodoListByUuid(updateTodoListRequest.getTodoListUuid())==null)
            {
                errors.add(new ErrorPayload("story","Cannot find any story with this  uuid"));
                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {
                td.setTodoDate(updateTodoListRequest.getTodoDate());
                td.setUuid(updateTodoListRequest.getTodoListUuid());
                todoListService.updateTodoList(td);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("To do-list","Cannot find any todo List with uuid ="+updateTodoListRequest.getTodoListUuid()));
            return new ResponseEntity<>(new ErrorResponse(400,"To do-List not found  Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new SuccessResponse(201,"TodoList  update successfully... ",requestTime),HttpStatus.CREATED);

    }


//    @PatchMapping("updatePrivacy-todolist")
//    @Operation(summary = "Update to do-list privacy")
//    ResponseEntity<?> updatePrivacyList(@RequestParam String todoListUuid, @RequestParam  boolean privacy) {
//        String msg= "";
//        errors.removeAll(errors);
//        try{
//
//            if(todoListService.findTodoListByUuid(todoListUuid)==null)
//            {
//                errors.add(new ErrorPayload("story","Cannot find any story with this  uuid"));
//                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//
//            }else
//            {
//              int resutl = todoListService.updatePrivacyTodoList(todoListUuid,privacy);
//                System.out.println("done "+ resutl);
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("To do-list","Cannot find any todo List with uuid ="+todoListUuid));
//            return new ResponseEntity<>(new ErrorResponse(400,"To do-List not found  Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//        }
//
//        return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully... ",requestTime),HttpStatus.CREATED);
//
//    }
//

    @PutMapping("updatePrivacy-todolist")
    @Operation(summary = "Update to do-list privacy")
    ResponseEntity<?> updatePrivacyList(@RequestBody UpdateStoryPrivacyRequest privacyRequest) {
        String msg= "";
        errors.removeAll(errors);
        try{

            if(todoListService.findTodoListByUuid(privacyRequest.getUuid())==null)
            {
                errors.add(new ErrorPayload("story","Cannot find any story with this  uuid"));
                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);

            }else
            {
                int resutl = todoListService.updatePrivacyTodoList(privacyRequest.getUuid(),privacyRequest.isPublic());
                System.out.println("done "+ resutl);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("To do-list","Cannot find any todo List with uuid ="+privacyRequest.getUuid()));
            return new ResponseEntity<>(new ErrorResponse(400,"To do-List not found  Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully... ",requestTime),HttpStatus.CREATED);

    }















//
//    @PatchMapping("update-complete-status")
//    @Operation(summary = "Update complete status")
//    ResponseEntity<?> updateCompleteStatus(@RequestParam String todoListUuid) {
//        String msg= "";
//        errors.removeAll(errors);
//        try{
//            if(todoListService.findTodoByUuid(todoListUuid)==null)
//            {
//                errors.add(new ErrorPayload("To do ","Cannot find any To do with uuid = "+todoListUuid));
//                return new ResponseEntity<>(new ErrorResponse(400,"To do Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//
//            }else
//            {
//                if(todoListService.isComplete(todoListUuid)==false)
//                {
//                    todoListService.updateComplete(todoListUuid,true);
//                    msg="Todo status have been update To complete successfully";
//                }else {
//                    todoListService.updateComplete(todoListUuid,false);
//                    msg="Todo status have been update To not complete successfully";
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("To do", "There are problem while trying to update Todo"));
//            return new ResponseEntity<>(new ErrorResponse(400,"Error with DataBase",requestTime,errors),HttpStatus.NOT_FOUND);
//        }
//
//        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime),HttpStatus.CREATED);
//
//    }


    @PatchMapping("update-complete-status")
    @Operation(summary = "Update complete status")
    ResponseEntity<?> updateCompleteStatus(@RequestBody UpdateCommpleteStatusRequest up) {
        String msg= "";
        errors.removeAll(errors);
        try{
            if(todoListService.findTodoByUuid(up.getTodoUuid())==null)
            {
                errors.add(new ErrorPayload("To do ","Cannot find any To do with uuid = "+up.getTodoUuid()));
                return new ResponseEntity<>(new ErrorResponse(400,"To do Not Found",requestTime,errors),HttpStatus.NOT_FOUND);

            }else
            {
                if(todoListService.isComplete(up.getTodoUuid())==false)
                {
                    todoListService.updateComplete(up.getTodoUuid(),true);
                    msg="Todo status have been update To complete successfully";
                }else {
                    todoListService.updateComplete(up.getTodoUuid(),false);
                    msg="Todo status have been update To not complete successfully";
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("To do", "There are problem while trying to update Todo"));
            return new ResponseEntity<>(new ErrorResponse(400,"Error with DataBase",requestTime,errors),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime),HttpStatus.CREATED);

    }





    @DeleteMapping("delete-todo")
    @Operation(summary = "delete To do the child of To do-list")
    ResponseEntity<?> deleteTodo(@RequestParam String todoUuid) {
        errors.removeAll(errors);

        try{

            if(todoListService.findTodoByUuid(todoUuid)==null)
            {

                errors.add(new ErrorPayload("To do ","Cannot find any To do with uuid = "+todoUuid));
                return new ResponseEntity<>(new ErrorResponse(400,"To do Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }else {
                todoListService.deleteTodoByUuid(todoUuid);
            }
        }catch (Exception e)
        {
            errors.add(new ErrorPayload("Error","There are some error while try to delete Todo"));
            return new ResponseEntity<>(new ErrorResponse(400,"Fail to Delete To do with uuid"+todoUuid,requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Delete Todo successfully... ",requestTime),HttpStatus.OK);
    }


    @DeleteMapping("delete-todo-list")
    @Operation(summary = "delete To do-list")
    ResponseEntity<?> deleteTodoList(@RequestParam String todoListUuid) {
        errors.removeAll(errors);

        try{

            if(todoListService.findTodoListByUuid(todoListUuid)==null)
            {

                errors.add(new ErrorPayload("To do-list ","Cannot find any To do-list with uuid = "+todoListUuid));
                return new ResponseEntity<>(new ErrorResponse(400,"To do-list Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }else {
                todoListService.deleteTodoListByUuid(todoListUuid);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error","There are some error while try to delete Todo"));
            return new ResponseEntity<>(new ErrorResponse(400,"Fail to Delete To List with uuid"+todoListUuid,requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Delete Todo-List successfully... ",requestTime),HttpStatus.OK);
    }



    //   version 2
    @GetMapping("get-all-todo-list-by-user-uid")
    @Operation(summary = "get all todo-list by userUuid")
    ResponseEntity<?> getallJourneyByUserUuid(@RequestParam String userUuid ,@RequestParam  (defaultValue = "10") int limit,
                                              @RequestParam(defaultValue = "1") int page) {
        String msg= "";
        List<GetAllTodoListPayload> getAllTodoListByUserUuid=new ArrayList<>();
        try {
            Paging paging = new Paging ( );
            paging.setPage ( page );
            int offset = (page - 1) * limit;
            paging.setLimit ( limit );

            int userID = userService.getUserIdByUuid(userUuid);
            System.out.println("Here is the user id" + userID);
                getAllTodoListByUserUuid=todoListService.getAllTodolistByUserUuid(userID,limit,offset);
            System.out.println("Here  is the  to do list"+ getAllTodoListByUserUuid);


            if(getAllTodoListByUserUuid==null)
            {
                msg="No Todo List to show ...";
            }
            else {
                msg="Todo List was fetch successfully";
            }
        } catch ( Exception ex ) {
            ex.printStackTrace();
            System.out.println ( " Fetching data exception: " + ex.getMessage ( ) );
            return new ResponseEntity<>(new ErrorResponse(404,"There are some problem while fetching data",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new GetAllTodoListByUserUuidResponse(201,msg,requestTime,getAllTodoListByUserUuid),HttpStatus.FOUND);
    }























}
