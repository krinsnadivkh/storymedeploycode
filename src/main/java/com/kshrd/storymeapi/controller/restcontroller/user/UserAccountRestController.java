package com.kshrd.storymeapi.controller.restcontroller.user;

import com.kshrd.storymeapi.dto.payload.ErrorPayload;
import com.kshrd.storymeapi.dto.payload.FollowingResponePayload;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;
import com.kshrd.storymeapi.dto.request.users.FollowAndUnfollowRequest;
import com.kshrd.storymeapi.dto.request.users.UpdateProfileRequest;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.story.IsLikeResponse;
import com.kshrd.storymeapi.dto.response.users.CountFollowerResponse;
import com.kshrd.storymeapi.dto.response.users.GetAllFollowerResponse;
import com.kshrd.storymeapi.dto.response.users.UserInfoResponse;
import com.kshrd.storymeapi.dto.response.users.WhoToFollowResponse;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.customfuntion.CustomFuntion;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/user-account")
public class UserAccountRestController {
    Date requestTime = new Date();
    @Autowired
    UserService userService;
    String curentUserEmail= "";
    CustomFuntion  customFuntion = new CustomFuntion();
    List<ErrorPayload> errors = new ArrayList<>();
    @PatchMapping("update-user-profile")
    @Operation(summary = "Update user profile")
    ResponseEntity<?> updateUserProfile(@RequestBody @Valid UpdateProfileRequest updateProfileRequest){
        String msg= "";
        int a;
        errors.removeAll(errors);
        System.out.println(updateProfileRequest);
        UpdateProfileRequest userData =new UpdateProfileRequest();
        userData.setUserUuid(updateProfileRequest.getUserUuid());
        userData.setBio(updateProfileRequest.getBio());
        userData.setFullName(updateProfileRequest.getFullName());
        userData.setEmail(updateProfileRequest.getEmail());
        userData.setGender(updateProfileRequest.getGender());
        userData.setImage(updateProfileRequest.getImage());
        userData.setDateOfBirth(updateProfileRequest.getDateOfBirth());
            if(userService.finduserByUuid(updateProfileRequest.getUserUuid())==null)
                    {
                        errors.add(new ErrorPayload("user", "Can not find any user with this email..."));
                        return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
                    }
            else {
                try
                {
                    a= userService.updateUserProfile(userData);
                    System.out.println("here is theresilt"+a);
                    System.out.println("hUser data"+userData);
                }
                catch (Exception e)

                {
                    errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to update profile !!!"));
                    return new ResponseEntity<>(new ErrorResponse(404,"Cannot Update!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
                }
            }


        return new ResponseEntity<>(new SuccessResponse(201,"User profile Update Successfully",requestTime), HttpStatus.OK);
    }


//
//    @PostMapping("follow-and-unfollow-other-user")
//    @Operation(summary = "follow and unfollow")
//    ResponseEntity<?> followAndUnfollowOtherUser(@RequestParam int otherUserId) {
//        errors.removeAll(errors);
//        String msg= "";
//        int ownerId = 0;
//        try
//        {
//          curentUserEmail=  customFuntion.getCurrentUser();
//            if(curentUserEmail=="anonymousUser")
//            {
//                errors.add(new ErrorPayload("Login Required","You have  not login you can not access this resource !!!"));
//                return new ResponseEntity<>(new ErrorResponse(404,"Not allow ",requestTime,errors),HttpStatus.BAD_REQUEST);
//            }
//            else {
//              ownerId=  userService.getUserIdByEmail(curentUserEmail);
//                System.out.println("Here is the ower id"+ownerId);
//              if(ownerId==0)
//              {
//                  errors.add(new ErrorPayload("Users", "Sorry cannot find any user with this email !!!"));
//                  return new ResponseEntity<>(new ErrorResponse(404,"User not found !!! ",requestTime,errors),HttpStatus.BAD_REQUEST);
//              }
//              else
//              {
//                   if(userService.isFollow(ownerId,otherUserId)>0)
//                   {
//                        userService.unFollowAuthor(ownerId,otherUserId);
//                        msg="Unfollow user successfully...";
//                   }
//                   else {
//                       userService.followAuthor(ownerId,otherUserId);
//                       msg="Follow user successfully...";
//                   }
//              }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to follow"));
//            return new ResponseEntity<>(new ErrorResponse(404,"Cannot follow!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime), HttpStatus.OK);
//    }


    @PostMapping("follow-and-unfollow-other-user")
    @Operation(summary = "follow and unfollow")
    ResponseEntity<?> followAndUnfollowOtherUser(@RequestBody FollowAndUnfollowRequest fu) {
        errors.removeAll(errors);
        String msg= "";
        int ownerId = 0;
        try
        {
            curentUserEmail=  customFuntion.getCurrentUser();
            if(curentUserEmail=="anonymousUser")
            {
                errors.add(new ErrorPayload("Login Required","You have  not login you can not access this resource !!!"));
                return new ResponseEntity<>(new ErrorResponse(404,"Not allow ",requestTime,errors),HttpStatus.BAD_REQUEST);
            }
            else {
                ownerId=  userService.getUserIdByEmail(curentUserEmail);
                System.out.println("Here is the ower id"+ownerId);
                if(ownerId==0)
                {
                    errors.add(new ErrorPayload("Users", "Sorry cannot find any user with this email !!!"));
                    return new ResponseEntity<>(new ErrorResponse(404,"User not found !!! ",requestTime,errors),HttpStatus.BAD_REQUEST);
                }
                else
                {
                    if(userService.isFollow(ownerId,fu.getOtherUserId())>0)
                    {
                        userService.unFollowAuthor(ownerId,fu.getOtherUserId());
                        msg="Unfollow user successfully...";
                    }
                    else {
                        userService.followAuthor(ownerId,fu.getOtherUserId());
                        msg="Follow user successfully...";
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to follow"));
            return new ResponseEntity<>(new ErrorResponse(404,"Cannot follow!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime), HttpStatus.OK);
    }












    @GetMapping("get-all-following")
    @Operation(summary = "get all following")
    ResponseEntity<?> getAllFollowing(@RequestParam int userId) {
        List<FollowingResponePayload>allFollowing =  new ArrayList<>();
        String msg= "";
        try
        {
            if(userService.finduserById(userId)==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this email"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

                List<Integer> followerId= userService.getFollowingId(userId);
                for (int i:followerId) {
                    allFollowing.add(userService.getSingleFollowingById(i));
                }
                if(allFollowing.isEmpty())
                {
                    msg="No following to show !!!";
                }else {
                    msg="Following fetching successfully";
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to follow"));
            return new ResponseEntity<>(new ErrorResponse(404,"Cannot follow!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new GetAllFollowerResponse(202,msg,requestTime, allFollowing), HttpStatus.OK);
    }

    @GetMapping("get-all-follower")
    @Operation(summary = "get all follower")
    ResponseEntity<?> getAllFollower(@RequestParam int userId) {
        List<FollowingResponePayload>allFollowing =  new ArrayList<>();
        String msg= "";
        try
        {
            if(userService.finduserById(userId)==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this email"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

                List<Integer> followerId= userService.getFollowerId(userId);
                for (int i:followerId) {
                    allFollowing.add(userService.getSingleFollowerById(i));
                }
                if(allFollowing.isEmpty())
                {
                    msg="No follower to show !!!";
                }else {
                    msg="Follower fetching successfully";
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying fetching follower"));
            return new ResponseEntity<>(new ErrorResponse(404,"Cannot fetching data!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new GetAllFollowerResponse(202,msg,requestTime, allFollowing), HttpStatus.OK);
    }



    @GetMapping("count-follwer")
    @Operation(summary = "count total follower")
    ResponseEntity<?> countFollower(@RequestParam int userId) {
        String msg= "";
        int totalFlollwer;
        try
        {
            if(userService.finduserById(userId)==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this email"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

               totalFlollwer = userService.countFollower(userId);
                System.out.println("total"+totalFlollwer);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Fail to fetch data ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new CountFollowerResponse(202,"Total Follower fetch successfully",requestTime,totalFlollwer),HttpStatus.OK);

    }



    @GetMapping("count-following")
    @Operation(summary = "count total following")
    ResponseEntity<?> countFollowing(@RequestParam int userId) {
        String msg= "";
        int totalFollowing;
        try
        {
            if(userService.finduserById(userId)==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this email"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

                totalFollowing = userService.countFollowing(userId);
                System.out.println("total"+totalFollowing);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Fail to fetch data ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new CountFollowerResponse(202,"Total Following fetch successfully",requestTime,totalFollowing),HttpStatus.OK);

    }


    @GetMapping("get-user-detail-byUuid")
    @Operation(summary = "get user info by user uuid")
    ResponseEntity<?> getUserDetailByUuid(@RequestParam String uuid) {
        String msg= "";
        UserInfoPayload user = new UserInfoPayload();
        try
        {
            user = userService.getUserInfo(uuid);
            if(user==null)
            {
                errors.add(new ErrorPayload("User", "Can not find any user with this uuid"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {

            msg="User info Fetching successfully";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to query data from database !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Fail to fetch data ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new UserInfoResponse(202,msg,requestTime,user),HttpStatus.OK);

    }


//    new versoin
@GetMapping("isFollow")
@Operation(summary = "isFollow")
ResponseEntity<?> isFollow(@RequestParam int otherUserId) {
    errors.removeAll(errors);
    String msg= "";
    int ownerId = 0;
    boolean check=false;
    try
    {
        curentUserEmail=  customFuntion.getCurrentUser();
        if(curentUserEmail=="anonymousUser")
        {
            errors.add(new ErrorPayload("Login Required","You have  not login you can not access this resource !!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Not allow ",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        else {
            ownerId=  userService.getUserIdByEmail(curentUserEmail);
            System.out.println("Here is the ower id"+ownerId);
            if(ownerId==0)
            {
                errors.add(new ErrorPayload("Users", "Sorry cannot find any user with this email !!!"));
                return new ResponseEntity<>(new ErrorResponse(404,"User not found !!! ",requestTime,errors),HttpStatus.BAD_REQUEST);
            }
            else
            {
                if(userService.isFollow(ownerId,otherUserId)>0)
                {
                    msg="Already follow this user ...";
                    check=true;
                }
                else {
                    userService.followAuthor(ownerId,otherUserId);
                    msg="Not follow this user yet...\"";
                    check=false;
                }
            }
        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
        errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to follow"));
        return new ResponseEntity<>(new ErrorResponse(404,"Cannot follow!!!",requestTime,errors),HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(new IsLikeResponse(201,msg,requestTime,check), HttpStatus.OK);
}


//sna verison 2 who to follow
@GetMapping("who-to-follow")
@Operation(summary = "get who to follow")
ResponseEntity<?> getWhoToFollow() {
    String msg= "";
    UserInfoPayload user = new UserInfoPayload();
    List<UserInfoPayload> whoToFollow= new ArrayList<>();
    try
    {
whoToFollow=userService.getWhoToFolow();
        if(whoToFollow==null)
        {
            errors.add(new ErrorPayload("User", "No User to display!!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"User not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }
        else {

            msg="User info Fetching successfully";
        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
        errors.add(new ErrorPayload("Error", "Sorry their are problem while trying to query data from database !!!"));
        return new ResponseEntity<>(new ErrorResponse(404,"Fail to fetch data ",requestTime,errors),HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(new WhoToFollowResponse(202,msg,requestTime,whoToFollow),HttpStatus.OK);

}







}
