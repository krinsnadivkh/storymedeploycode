package com.kshrd.storymeapi.controller.restcontroller.journey;

import com.kshrd.storymeapi.dto.payload.ErrorPayload;
import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.dto.request.journey.JourneyRequest;
import com.kshrd.storymeapi.dto.request.journey.UpdateJourneyRequest;
import com.kshrd.storymeapi.dto.request.sotry_post.StoryPostRequest;
import com.kshrd.storymeapi.dto.request.sotry_post.UpdateStoryPrivacyRequest;
import com.kshrd.storymeapi.dto.response.ErrorResponse;
import com.kshrd.storymeapi.dto.response.SuccessResponse;
import com.kshrd.storymeapi.dto.response.journey.CreateJouneyResponse;
import com.kshrd.storymeapi.dto.response.journey.GetAllJourneyResponse;
import com.kshrd.storymeapi.dto.response.journey.JourneyResponse;
import com.kshrd.storymeapi.dto.response.users.UserLoginResponse;
import com.kshrd.storymeapi.model.journey.Journey;
import com.kshrd.storymeapi.model.users.Users;
import com.kshrd.storymeapi.service.journey.JourneyService;
import com.kshrd.storymeapi.service.users.UserService;
import com.kshrd.storymeapi.utilities.paging.Paging;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/journey")
public class JourneyRestController {
    Date requestTime = new Date();
    @Autowired
    JourneyService journeyService;
    @Autowired
    UserService userService;
     List<ErrorPayload> errors = new ArrayList<>();
    public static String getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        System.out.println(principal);
        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
            System.out.println("if"+username);
        } else {
            username = principal.toString();
            System.out.println("else"+username);
        }
        return username;
    }

    @PostMapping("create-journey")
    @Operation(summary = "crate journey")
    ResponseEntity<?> createJourney(@RequestBody @Valid JourneyRequest journeyRequest) {
        String msg= "";
        Journey journey= new Journey();
        GetAllJourneyPayload journeyRes= new GetAllJourneyPayload();
        System.out.println("here is request"+journeyRequest);
        journey.setTitle(journeyRequest.getTitle());
        journey.setDescription(journeyRequest.getDescription());
        journey.setImage(journeyRequest.getImage());
        journey.setPublic(journeyRequest.isPublis());
        journey.setUserId(journeyRequest.getUserId());
        System.out.println(journeyRequest.getUserId());
        System.out.println("b"+journeyRequest.isPublis());
    Users ckUser= userService.finduserById(journeyRequest.getUserId());
    if(ckUser==null)
    {
        errors.add(new ErrorPayload("User","Can not find any user with this Id!!!"));
        return new ResponseEntity<>(new ErrorResponse(404,"Can not create journey !!",requestTime,errors),HttpStatus.BAD_REQUEST);
    }
    if (journeyRequest.getTitle().isEmpty()||journeyRequest.getDescription().isEmpty()||journeyRequest.getUserId()==0)
    {
        errors.add(new ErrorPayload("All Field","All field are required before summit. Except for image only !!!"));
        return new ResponseEntity<>(new ErrorResponse(404,"Input Required",requestTime,errors),HttpStatus.BAD_REQUEST);
    }
    else
        {
            try {
                int journeyId= journeyService.createJourney(journey);
                if(journeyId!=0)
                {
                    journeyRes = journeyService.getJourneyById(journeyId);

                }
            }catch (Exception e)
            {
                e.printStackTrace();
                errors.add(new ErrorPayload("Error","Error while insert to database !!!"));
                return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating journey",requestTime,errors),HttpStatus.BAD_REQUEST);
            }
        }
        msg="Journey was create successfully...";
        return new ResponseEntity<>(new CreateJouneyResponse(201,msg,requestTime,journeyRes),HttpStatus.OK);
    }


    @PostMapping("get-all-journey")
    @Operation(summary = "get all journey")
    ResponseEntity<?> getAllJourney(@RequestParam(defaultValue = "10") int limit,
                                    @RequestParam(defaultValue = "1") int page) {
        String msg= "";
//getCurrentUser();
       List<GetAllJourneyPayload> findAllJourney=new ArrayList<>();
        System.out.println(findAllJourney);
        try {

            Paging paging = new Paging ( );
            paging.setPage ( page );
            int offset = (page - 1) * limit;
            paging.setLimit ( limit );
            findAllJourney=journeyService.getAllJourney(limit,offset);

        } catch ( Exception ex ) {
            System.out.println ( " Fetching data exception: " + ex.getMessage ( ) );
            return new ResponseEntity<>(new ErrorResponse(404,"Error found will fetching data",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        msg="Journey was create succufully...";
        return new ResponseEntity<>(new GetAllJourneyResponse(201,"Journey was fetch successfully",requestTime,findAllJourney),HttpStatus.FOUND);
    }


    @PatchMapping("update-journey")
    @Operation(summary = "update journey")
    ResponseEntity<?> updateJourney(@RequestBody @Valid UpdateJourneyRequest journeyRequest) {
        String msg= "";
        Journey journey= new Journey();
        System.out.println("here is request"+journeyRequest);
        journey.setTitle(journeyRequest.getTitle());
        journey.setDescription(journeyRequest.getDescription());
        journey.setImage(journeyRequest.getImage());
        journey.setPublic(journeyRequest.isPublis());
        journey.setUuid(journeyRequest.getUuid());
        System.out.println("b"+journeyRequest.isPublis());
Journey ckJourney = journeyService.findJourneyByUuid(journeyRequest.getUuid());

        if(ckJourney==null)
        {
            errors.add(new ErrorPayload("Journey","Cannot find any journey with this Uuid!!!"));
            return new ResponseEntity<>(new ErrorResponse(404,"Journey not found !!!",requestTime,errors),HttpStatus.NOT_FOUND);
        }else {
            System.out.println("story found");
        }

        if (journeyRequest.getTitle().isEmpty()||journeyRequest.getDescription().isEmpty()||journeyRequest.getUuid().isEmpty())
        {
            errors.add(new ErrorPayload("All Field","All field are required before summit."));
            return new ResponseEntity<>(new ErrorResponse(404,"Input Required",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        else
        {
            try {
                int updateResult= journeyService.updateJourney(journey);
                System.out.println("re"+ updateResult);
            }catch (Exception e)
            {
                e.printStackTrace();
                errors.add(new ErrorPayload("Error","Error while insert to database !!!"));
                return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem creating journey",requestTime,errors),HttpStatus.BAD_REQUEST);
            }
        }
        msg="Journey was create succufully...";
        return new ResponseEntity<>(new SuccessResponse(201,msg,requestTime),HttpStatus.OK);
    }

    @DeleteMapping("delete-journey")
    @Operation(summary = "delete journey")
    ResponseEntity<?> deleteJourney(@RequestParam @NotNull String uuid) {
        errors.removeAll(errors);
        try{
           Journey result = journeyService.findJourneyByUuid(uuid);
            if(result==null)
            {
                errors.add(new ErrorPayload("post","Cannot find any post with uuid = "+uuid));
                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {
              int a=  journeyService.deleteJourney(uuid);
                System.out.println(a);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("journey","There are some eror while try to delete journey"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to delete the journey",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Delete journey successfully... ",requestTime),HttpStatus.CREATED);
    }

//    @PatchMapping("updatePrivacy-journey")
// @Operation(summary = "update privacy")
//    ResponseEntity<?> updatePrivacyJourney(@RequestParam String uuid, @RequestParam  boolean privacy) {
//        errors.removeAll(errors);
//        try{
//            Journey result = journeyService.findJourneyByUuid(uuid);
//            if(result==null)
//            {
////                System.out.println("can not find any post with uuid = "+uuid);
//                errors.add(new ErrorPayload("post","Cannot find any post with uuid = "+uuid));
//                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
//            }
//            else {
//                Journey j = new Journey();
//                j.setUuid(uuid);
//                j.setPublic(privacy);
//                int a=  journeyService.updateJourneyPrivaxy(j);
//                System.out.println(a);
//            }
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//            errors.add(new ErrorPayload("journey","There are some eror while try to update d journey"));
//            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to update the privacy of journey",requestTime,errors),HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully journey successfully... ",requestTime),HttpStatus.CREATED);
//    }

//    sna

    @PutMapping("updatePrivacy-journey")
 @Operation(summary = "update privacy")
    ResponseEntity<?> updatePrivacyJourney(@RequestBody UpdateStoryPrivacyRequest PrivacyRequest) {
        errors.removeAll(errors);
        try{
            Journey result = journeyService.findJourneyByUuid(PrivacyRequest.getUuid());
            if(result==null)
            {
//                System.out.println("can not find any post with uuid = "+uuid);
                errors.add(new ErrorPayload("post","Cannot find any post with uuid = "+PrivacyRequest.getUuid()));
                return new ResponseEntity<>(new ErrorResponse(400,"Story Not Found",requestTime,errors),HttpStatus.NOT_FOUND);
            }
            else {
                Journey j = new Journey();
                j.setUuid(PrivacyRequest.getUuid());
                j.setPublic(PrivacyRequest.isPublic());
                int a=  journeyService.updateJourneyPrivaxy(j);
                System.out.println(a);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            errors.add(new ErrorPayload("journey","There are some eror while try to update d journey"));
            return new ResponseEntity<>(new ErrorResponse(400,"Sorry there are problem while trying to update the privacy of journey",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new SuccessResponse(201,"Privacy update successfully journey successfully... ",requestTime),HttpStatus.CREATED);
    }




    
//    version 2

    //    version 2
    @GetMapping("get-all-journey-by-user-uid")
    @Operation(summary = "get all journey by userUuid")
    ResponseEntity<?> getallJourneyByUserUuid(@RequestParam String userUuid ,@RequestParam  (defaultValue = "10") int limit,
                                              @RequestParam(defaultValue = "1") int page) {
        String msg= "";
        List<GetAllJourneyPayload> getAllJourneyByUserUuid=new ArrayList<>();
        System.out.println(getAllJourneyByUserUuid);
        try {
            Paging paging = new Paging ( );
            paging.setPage ( page );
            int offset = (page - 1) * limit;
            paging.setLimit ( limit );

            int userID = userService.getUserIdByUuid(userUuid);
            System.out.println("Here is the user id" + userID);

            getAllJourneyByUserUuid=journeyService.getAllJourneyByUserUuid(userID,limit,offset);
            System.out.println("Here is the all jjour "+ getAllJourneyByUserUuid);
    if(getAllJourneyByUserUuid==null)
    {
        msg="No journey to show ...";
    }
    else {
        msg="Journey was fetch successfully";
    }
        } catch ( Exception ex ) {
            ex.printStackTrace();
            System.out.println ( " Fetching data exception: " + ex.getMessage ( ) );
            return new ResponseEntity<>(new ErrorResponse(404,"There are some problem while fetching data",requestTime,errors),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new GetAllJourneyResponse(201,msg,requestTime,getAllJourneyByUserUuid),HttpStatus.FOUND);
    }
















}
