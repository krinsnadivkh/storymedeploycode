package com.kshrd.storymeapi.controller.restcontroller.fileUpload;

import com.kshrd.storymeapi.dto.response.file.UploadFileRespone;
import com.kshrd.storymeapi.service.fileservice.FileStorageService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController

//@RequiredArgsConstructor
@RequestMapping("/api/v1/file")

public class FileUplaodController {

    @Autowired
    FileStorageService fileStorageService;

    @PostMapping(value = "/file-upload",consumes = "multipart/form-data")
    @Operation(summary = "upload file")
    public UploadFileRespone uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);
        String fileUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/images/")
                .path(fileName)
                .toUriString();
        return new UploadFileRespone (fileName, fileUrl,
                file.getContentType(), file.getSize());
    }
}

