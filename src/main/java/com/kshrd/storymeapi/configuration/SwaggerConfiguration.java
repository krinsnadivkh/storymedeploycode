package com.kshrd.storymeapi.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {
@Bean
    public OpenAPI customOpenAPI(){
    return new OpenAPI()
            .addSecurityItem(
                    new SecurityRequirement()
                            .addList("bearerAuth")
            )
            .components(
                    new Components()
                            .addSecuritySchemes("bearerAuth",
                                    new SecurityScheme()
                                            .name("bearerAuth")
                                            .type(SecurityScheme.Type.HTTP)
                                            .bearerFormat("JWT")
                                            .scheme("bearer")
                                    )
            )
            .info(
                    new Info()
                            .title("StoryMe")
                            .description("Create your own beautiful story")
                            .version("v1.0")
                            .contact(
                                    new Contact()
                                            .name("Story Me API Team ")
                                            .url("https://kshrd.com.kh")
                                            .email("trykrisnapcu@gmail.com")

                            )
                            .termsOfService("TOC")
                            .license(new License().name("License").url("https://kshrd.com.kh"))
            );


}

}
