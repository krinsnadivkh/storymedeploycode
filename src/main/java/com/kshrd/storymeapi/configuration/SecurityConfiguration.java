package com.kshrd.storymeapi.configuration;

import com.kshrd.storymeapi.security.UserDetailsServiceImp;
import com.kshrd.storymeapi.security.jwt.JwtAuthenticationEntryPoint;
import com.kshrd.storymeapi.security.jwt.JwtTokenFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private final UserDetailsServiceImp userDetailsService;
    @Autowired
    JwtTokenFilter jwtTokenFilter;

    @Autowired
    private SecurityConfiguration(UserDetailsServiceImp userDetailsService,JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint)
    {
        this.userDetailsService= userDetailsService;
        this.jwtAuthenticationEntryPoint= jwtAuthenticationEntryPoint;
    }


    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder ( );
    }
    @Bean
    public AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService ( userDetailsService )
                .passwordEncoder ( passwordEncoder ( ) );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
                .and()
//                .antMatchers("/api/v1/story-post/react-story/**")
//                .hasAnyRole("USER")
                .authorizeRequests()
                .antMatchers("/api/v1/auth/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/like-story/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/get-all-popular-story/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/get-recentStory/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/search-story-by-title/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/get-story-by-uuid/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/getUserOfTheMonth/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/getUserOfTheWeek/**")
                .permitAll()
                .antMatchers("/api/v1/user-account/who-to-follow/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/search-story-by-type/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/search-user-by-name/**")
                .permitAll()
                .antMatchers("/api/v1/user-account/get-user-detail-byUuid/**")
                .permitAll()

                .antMatchers("/api/v1/user-account/get-all-following/**")
                .permitAll()

                .antMatchers("/api/v1/user-account/get-all-follower/**")
                .permitAll()
                .antMatchers("/api/v1/story-post/getAllByUseId/**")
                .permitAll()






                .antMatchers("/api/v1/story-post/**")
//                .permitAll()
                .hasAnyRole("USER")
//                .antMatchers("/api/v1/story-post/react-story/**")
//                .hasAnyRole("USER")
//                .permitAll()
                .antMatchers("/api/v1/journey/**")
                .hasAnyRole("USER")

                .antMatchers("/api/v1/todo-list/**")
                .hasAnyRole("USER")

                .antMatchers("/api/v1/user-account/**")
                .hasAnyRole("USER","ADMIN")


                .antMatchers("/api/v1/admin/**")
                .hasAnyRole("ADMIN")

                    .anyRequest()
                        .permitAll();


//                .authenticated();

        // Add a filter to validate the tokens with every request
        http.addFilterBefore ( jwtTokenFilter, UsernamePasswordAuthenticationFilter.class );
    }




    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring ( )
                .antMatchers ( "/h2-console/**",
                        "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                        "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                        "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                        "/v3/api-docs/**", "/configuration/ui", "/configuration/security",
                        "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                        "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/" );
    }
}
