package com.kshrd.storymeapi.repository.admin;


import com.kshrd.storymeapi.dto.payload.AdminPayload;
import com.kshrd.storymeapi.dto.payload.GetAllStoryRequestPayload;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;
import com.kshrd.storymeapi.model.users.Users;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PatchMapping;

import java.sql.Struct;
import java.util.List;

@Mapper
@Repository
public interface AdminRepository {
//    private String uuid;
//    private String fullName;
//    private String profile;
//    private int  followCount;

    @Results(
            {

                    @Result(property = "id", column = "id"),
                    @Result(property = "userUuid", column = "user_uuid"),
                    @Result(property = "gender", column = "gender"),
                    @Result(property = "bio", column = "bio"),
                    @Result(property = "dob", column = "date_of_birth"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "following",column = "id",one = @One (select = "com.kshrd.storymeapi.repository.users.UserRepository.countFollowing")),
                    @Result(property = "follower",column = "id",one = @One (select = "com.kshrd.storymeapi.repository.users.UserRepository.countFollower")),
//                    @Result(property = "followCount",column = "id"),
            }
    )
    @Select("SELECT * from sm_users limit #{limit}  offset #{offset}")
    List<GetAllUserPayload> getAllUser(int limit,int offset);

//sna will modify
    @Select("SELECT  count(*) from  sm_users")
    int countAllUser();

    @Select("SELECT  count(*) from  sm_story_posts ;")
    int countAllPost();

    @Select("SELECT  count(*) from  sm_comment")
    int countAllComment();

    @Select("SELECT * from sm_users where email=#{email}")
    @Results(
            {
                    @Result(property = "uuid",column = "user_uuid"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "dateOfBirth",column = "date_of_birth"),
            }
    )
    AdminPayload getAdminInfo(String email);


    @Update("UPDATE  sm_users set status=#{status} where user_uuid=#{userUuid}")
    int banUser(String userUuid,boolean status);



    @Select("SELECT count(*) from sm_story_type where is_allow=false")
    int getTotalStoryRequest();

    @Update("UPDATE sm_story_type set is_allow=true where uuid=#{uuid}")
    int acceptStoryRequest(String uuid);

    @Delete("DELETE from  sm_story_type where uuid=#{uuid}")
    int deleteStoryRequest(String uuid);





    @Select("SELECT * from sm_story_type inner join sm_users su on sm_story_type.user_id = su.id where is_allow=false")
    @Results(
            {
                    @Result(property = "uuid",column = "user_uuid"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "storyRequest",column = "story_type"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "createDate",column = "creat_date"),
                    @Result(property = "storyUuid",column = "uuid"),
            }
    )
    List<GetAllStoryRequestPayload> getAllStoryRequest();














}
