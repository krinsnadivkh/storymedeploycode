package com.kshrd.storymeapi.repository.story;

import com.kshrd.storymeapi.dto.payload.*;
import com.kshrd.storymeapi.dto.request.story.ParagraphsUpdateRequest;
import com.kshrd.storymeapi.dto.response.story.StoryRespones;
import com.kshrd.storymeapi.dto.response.story.UpdateResponse;
import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import com.kshrd.storymeapi.model.story.Story;
import com.kshrd.storymeapi.model.story.RequestStory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Mapper
@Repository
public interface StoryRepository {

    @Select("SELECT count(*) from sm_likes where story_id=#{id}")
    int countLikeByStoryId(int id);

    @Select("SELECT count(*) from sm_comment where story_id=#{id}")
    int countLCommentByStoryId(int id);




    @Select("INSERT INTO sm_story_posts " +
            "(uuid, title, is_public, create_date,story_type_id, owner_uuid, emotion_id,veiw_count,like_count,comment_count,user_id) \n" +
            "VALUES (#{story.uuid}" +
            ",#{story.title}," +
            "" +
           "#{story.isPublic}," +
            "#{story.createDate}," +
            "#{story.storyTypeID}," +
            "#{story.ownerId}," +
            "#{story.emotionID},#{story.veiwCount},#{story.likeCount},#{story.commentCount},#{story.userId}) returning id")
    int crateStory(@Param("story") Story story);



    @Select("SELECT * from sm_story_posts inner join sm_users su on su.id = sm_story_posts.user_id where sm_story_posts.id=#{id}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "userProfile",column = "image"),
                    @Result(property = "userUuid",column = "user_uuid"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    GetStroyByUuidPayload getStoryCreateResponse(int id);













    @Select("Select * from sm_story_posts")
    StoryRespones getAllStory ();

//    Check dose type present or not

//    @Select("SELECT id from sm_story_type where story_type=#{storyType}")
    @Select("SELECT id from sm_story_type WHERE (UPPER(story_type) like UPPER('%'||#{storyType}||'%')) ")
    int getStoryTypeIDByName (String storyType);

//    crate paragraphs
    @Insert("INSERT INTO sm_paragraphs (uuid, paragraph, image, story_id) values (#{paragraph.uuid},#{paragraph.paragraph},#{paragraph.image},#{paragraph.storyId})")
    int crateParagraphs(@Param("paragraph") Paragraphs paragraph);

    @Select("SELECT * from sm_paragraphs where story_id=#{id} order by id")
    @Results(
            {
                    @Result(property = "storyId",column = "story_id"),
            })
    List<ParagraphsPayload> getAllParagraphsById(int id);
    @Select("SELECT name from sm_emotions where id=#{id}")
    String  getEmotionBYID (int id);

    @Select("SELECT story_type from sm_story_type where id=#{id}")
    String getStroyTypeById(int id);

    @Select("SELECT * from sm_comment inner join sm_users su on su.id = sm_comment.user_id where story_id=#{id} order by sm_comment.create_date")
    @Results(
            {
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "userName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "commentUuid",column = "uuid"),
            }
    )
    List<CommentPayload> getCommentByStoryId(int id);


    @Select("SELECT * from sm_story_posts inner join sm_users su on su.id = sm_story_posts.user_id where owner_uuid=#{uuid} order by sm_story_posts.id desc")
    @Results(
                    {
                            @Result(property = "id",column = "id"),
                            @Result(property = "storyUuid",column = "uuid"),
                            @Result(property = "createDate",column = "create_date"),
                            @Result(property = "ispublic",column = "is_public"),
                            @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),

//                            @Result(property = "fullName",column = "full_name"),
//                            @Result(property = "userProfile",column = "image"),
//                            @Result(property = "userUuid",column = "user_uuid"),
//
                            @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                            @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                            @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                            @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),

                            @Result(property = "veiwCount",column = "veiw_count"),

                            @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                            @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    }
            )
    List<GetAllPostByUserIDPayload> getStoryByUserId(String uuid);


//    Comment Section
//    @Insert("INSERT INTO sm_comment (uuid, content, create_date, story_id, user_id) VALUES (#{comment.uuid},#{comment.content},#{comment.createDate},#{comment.storyId},#{comment.userId})")
//    int crateComment(@Param("comment")Comments comment);

    @Select("INSERT INTO sm_comment (uuid, content, create_date, story_id, user_id) VALUES (#{comment.uuid},#{comment.content},#{comment.createDate},#{comment.storyId},#{comment.userId}) returning id")
    int crateComment(@Param("comment")Comments comment);

//    sna
    @Select("select * from sm_comment inner join sm_users su on su.id = sm_comment.user_id where sm_comment.id=#{id}")
    @Results(
            {
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "userName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "commentUuid",column = "uuid"),
            }
    )

    CommentPayload getCommentById(int id);




//    End Comment Section
    @Delete("DELETE FROM sm_story_posts where uuid=#{storyUuid}")
    int deleteStroy(String storyUuid);


    @Update("Update sm_story_posts set title=#{up.title},emotion_id=#{up.emotionID},story_type_id=#{up.storytypeId},is_public=#{up.isPublis} where uuid=#{up.postUuid}")
    int updateStroyByUuid(@Param("up") UpdateStoryPayload updateStoryPayload);
    @Select("SELECT * from sm_story_posts where uuid=#{uuid}")
    @Results(
            {
                    @Result(property = "postUuid",column = "uuid"),
                    @Result(property = "isPublis",column = "is_public"),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    UpdateResponse getStoryByStroyUuid(String uuid);

    @Select("SELECT * from sm_paragraphs where uuid=#{uuid}")
    Paragraphs findParagraphByUUid (String uuid);

    @Select("SELECT * from sm_paragraphs where id=#{id}")
    Paragraphs findParagraphById (int id);

    @Update("UPDATE  sm_paragraphs set paragraph=#{pa.paragraphs}, image=#{pa.image} where uuid=#{pa.uuid}")
    int updateParagraphsById(@Param("pa") ParagraphsUpdateRequest paragraphs);


    @Update("UPDATE  sm_paragraphs set paragraph=#{pa.paragraphs}, image=#{pa.image} where id=#{pa.id}")
    int updateParagraphsByMyId(@Param("pa") ParagraphsUpdateRequest paragraphs);


    @Update("UPDATE sm_story_posts set is_public=#{isPublic} where uuid=#{uuid}")
    int updatePrivacyByUuid(String uuid,Boolean isPublic);


    @Select("SELECT * from sm_story_posts where uuid=#{uuid}")
    Story findStroyByUuid(String uuid);
    @Select("SELECT * from sm_story_posts where id=#{id}")
    Story findStroyById(int id);

    @Insert("INSERT INTO  sm_likes ( story_id, user_id) VALUES (#{storyId},#{userId})")
    int likeStoryPost(int storyId,int userId);

    @Delete ( "DELETE from sm_likes where story_id=#{storyId} and user_id=#{userId}" )
    int unLikePost(int storyId, int userId);

    @Select ( "SELECT count(*) from sm_likes where story_id=#{storyId} and user_id=#{userId}" )
    int isLikePost(int storyId, int userId);



    @Update("UPDATE sm_story_posts set veiw_count=#{newVeiw} where uuid=#{uuid}")
    int incressView(String uuid,int newVeiw);

    @Select("SELECT veiw_count from sm_story_posts where uuid=#{uuid}")
    int getVeiwCount(String uuid);



    @Select("SELECT * from sm_story_posts inner join sm_users su on su.id = sm_story_posts.user_id where uuid=#{uuid}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "userProfile",column = "image"),
                    @Result(property = "userUuid",column = "user_uuid"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    GetStroyByUuidPayload getStoryByUuid(String storyUuid);


    @Insert("Insert Into sm_story_type (user_id,uuid, story_type, is_allow) VALUES (#{sr.userId},#{sr.uuid},#{sr.storyType},#{sr.isAllow})")
    int requestStoryType (@Param("sr") RequestStory storyRequest);


    @Select("SELECT * from  sm_users where id=#{id}")
    RecentUser getUserToReaderMore(int id);



//    @Select("SELECT * from  sm_story_posts where is_public=true order by create_date desc limit 100")
    @Select("SELECT * from  sm_story_posts where is_public=true order by id desc limit 100")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    List<RecentpostPayload> getRecentStory( );





    //    search by ite
//    @Select("SELECT * from  sm_story_posts where title=''")\ WHERE("UPPER(bio) like UPPER('%'||#{filter}||'%')");
    @Select("SELECT * from  sm_story_posts WHERE(UPPER(title) like UPPER('%'||#{title}||'%'))")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    List<SearchStoryPostPayload> searchStoryIbByTitle( String title);

    @Select("SELECT * from  sm_story_posts WHERE story_type_id= #{typeId}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    List<SearchStoryPostPayload> searchStoryByType( int typeId);


//    private String uuid;
//    private String name;
//    private String bio;
//    private String profile;
//    private String followerCount;

//    @Select("SELECT * FROM sm_users WHERE (UPPER(full_name) like UPPER('%'||'#{name}'||'%')) ")
//    @Results(
//            {
////                    @Result(property = "uuid", column = "user_uuid"),
////                    @Result(property = "name", column = "full_name"),
//////                    @Result(property = "bio", column = ""),
////                    @Result(property = "profile", column = "image"),
////                    @Result(property = "followerCount", column = "user_uuid"),
//            }
//    )

    @Select("SELECT * FROM sm_users WHERE (UPPER(full_name) like UPPER('%'||#{name}||'%')) and status=true;")
    @Results(

            {

                    @Result(property = "id", column = "id"),
                    @Result(property = "uuid", column = "user_uuid"),
                   @Result(property = "name", column = "full_name"),
                   @Result(property = "bio", column = "bio"),
                  @Result(property = "profile", column = "image"),
                   @Result(property = "followerCount", column = "id" ,one = @One(select = "com.kshrd.storymeapi.repository.users.UserRepository.countFollowing")),
//                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    List<SearchUserPayload> searchUserByName(String name);


    @Select("SELECT * FROM sm_story_posts  where is_public=true order by veiw_count desc  LIMIT 50 ")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
            }
    )
    List<RecentpostPayload> getPopularStory( );



    //v2 sna modify
    @Select("SELECT *  from sm_story_posts  inner join sm_users su on su.id = sm_story_posts.user_id where sm_story_posts.create_date=#{currentDate} and owner_uuid=#{userUuid}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "userProfile",column = "image"),
                    @Result(property = "userUuid",column = "user_uuid"),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),

                    @Result(property = "veiwCount",column = "veiw_count"),

                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
            }
    )


    List<MemorizeStoryPayload> getMemorizeOneYears(String currentDate,String userUuid);


//    v2
@Select("SELECT * from sm_story_posts where user_id =#{userId} and is_public=true order by create_date desc ")
@Results(
        {
                @Result(property = "id",column = "id"),
                @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                @Result(property = "storyUuid",column = "uuid"),
                @Result(property = "createDate",column = "create_date"),
                @Result(property = "ispublic",column = "is_public"),
                @Result(property = "veiwCount",column = "veiw_count"),
                @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
                @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
        }
)
List<RecentpostPayload> getAllFollowingStory(int userId );



@Select("Select following_id from  sm_following where user_id=#{userId}")
    List<Integer> getAllFollowingIDByUserId(int userId);




    @Select("SELECT  * from  sm_story_posts inner join sm_users su on su.id = sm_story_posts.user_id where sm_story_posts.create_date between #{start} and #{end} order by veiw_count desc limit 10")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
//                    @Result(property = "fullName",column = "full_name"),
//                    @Result(property = "userProfile",column = "image"),
//                    @Result(property = "userUuid",column = "user_uuid"),
//
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
            }
    )
    List<GetAllPostByUserIDPayload> getUserOfTheMonth(String start,String end);

    @Insert("insert into sm_user_achievement (user_id, achievement_id) values (#{user_id},#{achievementId})")
    int insertBestUser(int user_id,int achievementId);


//    @Select("SELECT * from sm_story_posts inner join sm_users su on su.id = sm_story_posts.user_id where user_id=#{userId} order by veiw_count asc limit 1")
    @Select("SELECT * from sm_story_posts where user_id=#{userId} order by veiw_count asc limit 1")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "storyUuid",column = "uuid"),
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "ispublic",column = "is_public"),
//                    @Result(property = "fullName",column = "full_name"),
//                    @Result(property = "userProfile",column = "image"),
//                    @Result(property = "userUuid",column = "user_uuid"),
                    @Result(property = "users",column = "user_id",one = @One(select = "getUserToReaderMore")),
                    @Result(property = "paragraphs",column = "id",many=@Many(select = "getAllParagraphsById")),
                    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByStoryId")),
                    @Result(property = "storyType",column = "story_type_id",one = @One (select = "getStroyTypeById")),
                    @Result(property = "emotion",column = "emotion_id",one = @One (select = "getEmotionBYID")),
                    @Result(property = "veiwCount",column = "veiw_count"),
                    @Result(property = "likeCount",column = "id",many = @Many(select = "countLikeByStoryId")),
                    @Result(property = "commentCount",column = "id",many = @Many(select = "countLCommentByStoryId")),
            }
    )
    GetAllPostByUserIDPayload getAllStoryByUserId(int userId);


//    get All story type
    @Select("SELECT story_type from sm_story_type where is_allow=true")
    List<String> getAllStoryType();

    @Select("SELECT * from sm_emotions")
    List<EmotionPayload> getAllEmotion();





}
