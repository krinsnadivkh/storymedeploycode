package com.kshrd.storymeapi.repository.journey;

import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.dto.payload.GetAllPostByUserIDPayload;
import com.kshrd.storymeapi.dto.request.journey.JourneyRequest;
import com.kshrd.storymeapi.model.journey.Journey;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface JourneyRepository {

//    @Insert("Insert Into sm_journeys (uuid, title, description, image, is_public, create_date, user_id) values " +
//            "(#{ju.uuid},#{ju.title},#{ju.description},#{ju.image},#{ju.isPublic},#{ju.createDate},#{ju.userId})")
//    int createJourney(@Param("ju") Journey journey);
//sna
@Select("Insert Into sm_journeys (uuid, title, description, image, is_public, create_date, user_id) values " +
        "(#{ju.uuid},#{ju.title},#{ju.description},#{ju.image},#{ju.isPublic},#{ju.createDate},#{ju.userId}) returning id")
int createJourney(@Param("ju") Journey journey);

    @Select("SELECT * from sm_journeys where id=#{id} order by create_date")
    @Results(
            {
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "isPublic",column = "is_public"),
            }
    )
    GetAllJourneyPayload getJourneyById(int id);












//    createJourney

    @Select("SELECT * from sm_journeys where uuid=#{uuid}")
    Journey findJourneyByUuid(String uuid);

    @Update("UPDATE sm_journeys set title=#{ju.title},description=#{ju.description},image=#{ju.image},is_public=#{ju.isPublic} where uuid=#{ju.uuid}")
    int updateJourney(@Param("ju") Journey journey);


    @Delete("DELETE FROM sm_journeys where uuid=#{uuid}")
    int deleteJourney(String uuid);

    @Update("UPDATE sm_journeys set is_public=#{ju.isPublic} where uuid=#{ju.uuid}")
    int updateJourneyPrivaxy(@Param("ju") Journey journey);

    @Select("SELECT * from sm_journeys  limit #{limit} offset #{offset} ")
    @Results(
            {
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "isPublic",column = "is_public"),
            }
    )
    List<GetAllJourneyPayload> getAllJourney(int limit,int offset);

    @Select("SELECT * from sm_journeys where user_id=#{userID} order by id desc  limit #{limit} offset #{offset}")
    @Results(
            {
                    @Result(property = "createDate",column = "create_date"),
                    @Result(property = "isPublic",column = "is_public"),
            }
    )
    List<GetAllJourneyPayload> getAllJourneyByUserUuid(int userID, int limit,int offset);




}
