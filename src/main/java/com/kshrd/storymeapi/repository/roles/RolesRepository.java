package com.kshrd.storymeapi.repository.roles;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RolesRepository {

//    Find user rolse by uer id
//select name from sm_role inner join sm_user_role sur on sm_role.id = sur.role_id where user_id=3
//    SELECT name FROM sm_role INNER JOIN sm_user_role smr on sm_role.id = smr.id WHERE user_id=#{id}
//    @Select("select name from sm_roles inner join sm_user_roles sur on sm_roles.id = sur.roles_id where user_id=#{id}")
    @Select("select name from sm_roles inner join sm_user_roles sur on sm_roles.id = sur.role_id where user_id=#{id}")
    List<String> findRolesById(int id);

    @Select("INSERT INTO sm_user_roles(user_id,role_id) values (#{userID},#{roleID}) returning id")
    int insertUserRole(int userID, int roleID);

}
