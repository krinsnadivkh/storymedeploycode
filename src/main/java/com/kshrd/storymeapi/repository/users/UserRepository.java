package com.kshrd.storymeapi.repository.users;

import com.kshrd.storymeapi.dto.payload.FollowerPayload;
import com.kshrd.storymeapi.dto.payload.FollowingResponePayload;
//import com.kshrd.storymeapi.dto.payload.UserResponePayload;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;
import com.kshrd.storymeapi.dto.request.users.UpdateProfileRequest;
import com.kshrd.storymeapi.model.users.AuthUser;
import com.kshrd.storymeapi.model.users.UserResponse;
import com.kshrd.storymeapi.model.users.Users;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface UserRepository {
//    select * from sm_users where id =2;
//    select  name  from sm_role inner join sm_user_role sur on sm_role.id = sur.role_id where user_id=2;
//    @Select("SELECT * FROM sm_users WHERE email =#{email}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "roles",column = "id",many = @Many(select = "com.kshrd.storymeapi.repository.roles.RolesRepository.findRolesById"))
//
//    })

    @Select("SELECT * FROM sm_users WHERE email=#{Email}")
    Users findByEmailFP(String Email);

    @Select("SELECT * FROM sm_users WHERE reset_password_token=#{ResetPasswordToken}")
    Users finduserByresetPasswordToken(String ResetPasswordToken);


    @Insert("UPDATE sm_users set reset_password_token =#{user.resetPasswordToken} where email=#{user.email}")
    int save(@Param("user") Users user);


    @Insert("UPDATE sm_users set password =#{user.password}, reset_password_token=#{user.resetPasswordToken}  where email=#{user.email}")
    int updatePassword(@Param("user") Users user);
//    int updatePassword(User user,String newPassword);

    @Select("SELECT reset_password_token FROM sm_users WHERE reset_password_token=#{token}")
    String getTokenByToken(String token);



//    End forget password section

    @Select("SELECT * FROM sm_users WHERE email =#{email}")
        @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles",column = "id",many = @Many(select = "com.kshrd.storymeapi.repository.roles.RolesRepository.findRolesById"))

    })
    Optional<UserResponse> findByEmailOp(String Email);


    @Select("SELECT * FROM sm_users WHERE email =#{email}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles",column = "id",many = @Many(select = "com.kshrd.storymeapi.repository.roles.RolesRepository.findRolesById"))

    })
   UserResponse findByEmail(String Email);








    @Select("SELECT * FROM sm_users WHERE email =#{email}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "profile", column = "image"),
            @Result(property = "userUuid", column = "user_uuid"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "bio", column = "bio"),
            @Result(property = "dob", column = "date_of_birth"),
            @Result(property = "roles",column = "id",many = @Many(select = "com.kshrd.storymeapi.repository.roles.RolesRepository.findRolesById"))
    })
    AuthUser findUserByEmail(String Email);

    @Select("insert into sm_users(full_name,user_uuid,email, password,date_of_birth,gender,create_date) values(#{user.full_name},#{user.uuid},#{user.email},#{user.password},#{user.date_of_birth},#{user.gender},#{user.createDate}) returning id;")
    int signUp(@Param("user") Users users);


    @Select("SELECT * from sm_users where user_uuid=#{uuid}")
    Users finduserByUuid (String uuid);

    @Select("SELECT id from sm_users where user_uuid=#{uuid}")
    int getUserIdByUuid (String uuid);

    @Select("SELECT * from sm_users where id=#{id}")
    Users finduserById (int id);





    @Select("SELECT  id from sm_users where email=#{email}")
    int getUserIdByEmail (String email);


//    @Insert("INSERT INTO  sm_likes ( story_id, user_id) VALUES (#{storyId},#{userId})")
//    int likeStoryPost(int storyId,int userId);
//
//    @Delete ( "DELETE from sm_likes where story_id=#{storyId} and user_id=#{userId}" )
//    int unLikePost(int storyId, int userId);
//
//    @Select ( "SELECT count(*) from sm_likes where story_id=#{storyId} and user_id=#{userId}" )
//    int isLikePost(int storyId, int userId);
//
@Insert("INSERT INTO sm_following (user_id,following_id) VALUES (#{ownerId},#{otherUserId})")
int followAuthor(int ownerId , int otherUserId);

    @Delete ( "DELETE  from  sm_following where user_id=#{ownerId} and following_id= #{otherUserId}" )
    int unFollowAuthor(int ownerId, int otherUserId);

    @Select ( "SELECT  count(*) from sm_following where user_id=#{ownerId} and following_id= #{otherUserId}" )
    int isFollow(int ownerId, int otherUserId);

    @Select("SELECT status from sm_users where user_uuid=#{status}")
    boolean isBan(String uuid);

    @Select("SELECT * from sm_following where user_id=#{userId}")
//    @Results(
//            {
//                    @Result(property = "allFollower",column = "following_id",many = @Many (select = "finduserById")),
//            }
//    )

    List<FollowerPayload> getAllFollowingBy (int userId);


//Following section
    @Select("SELECT following_id from sm_following where user_id=#{userId}")
    List<Integer> getFollowingId(int userId);
    @Select("SELECT count(*) from sm_following where user_id=#{ownerUuid}")
    int countFollowing(int ownerUuid);


    @Select("SELECT * from sm_users where id=#{id}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "uuid",column = "user_uuid"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "totalFollower",column = "id",one = @One (select = "countFollowing")),
            }
    )
    FollowingResponePayload getSingleFollowingById(int id);

//    Follower section

    @Select("SELECT user_id from sm_following where following_id=#{userId}")
    List<Integer> getFollowerId(int userId);
    @Select("SELECT count(*) from sm_following where following_id=#{ownerUuid}")
    int countFollower(int ownerUuid);
    @Select("SELECT count(*) from sm_following where user_uuid=#{#ownerUuid}")
    int countFollowerByUuid(String ownerUuid);


    @Select("SELECT * from sm_users where id=#{id}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "uuid",column = "user_uuid"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "totalFollower",column = "id",one = @One (select = "countFollowing")),
            }
    )
    FollowingResponePayload getSingleFollowerById(int id);

    @Update("UPDATE sm_users set full_name=#{u.fullName},gender=#{u.gender} ,image=#{u.image},email=#{u.email},date_of_birth=#{u.dateOfBirth},bio=#{u.bio} where user_uuid=#{u.userUuid}")
    int updateUserProfile(@Param("u") UpdateProfileRequest users);


//    private int id;
//    private String uuid;
//    private String fullName;
//    private String gender;
//    private String bio;
//    private String profile;
//    private int follower;
//    private int following;
//    private String email;
//    private LocalDateTime joinDate;
@Select("SELECT achievement from sm_achievement inner join sm_user_achievement sua on sm_achievement.id = sua.achievement_id where user_id=#{userId}")
List<String> getAchievementByUserId(int userId);


    @Select("SELECT * from sm_users WHERE user_uuid=#{uuid}")
    @Results(
            {
                    @Result(property = "id",column = "id"),
                    @Result(property = "uuid",column = "user_uuid"),
                    @Result(property = "fullName",column = "full_name"),
                    @Result(property = "profile",column = "image"),
                    @Result(property = "gender",column = "gender"),
                    @Result(property = "bio",column = "bio"),
                    @Result(property = "email",column = "email"),
                    @Result(property = "joinDate",column = "create_date"),
                    @Result(property = "DateOfBirth",column = "date_of_birth"),
                    @Result(property = "totalFollower",column = "id",one = @One (select = "countFollowing")),
                    @Result(property = "totalFollowing",column = "id",one = @One (select = "countFollower")),
                    @Result(property = "achievement",column = "id",many = @Many (select = "getAchievementByUserId")),
            })
    UserInfoPayload getUserInfo(String uuid);



    @Select("SELECT  count(*) from sm_user_achievement where user_id=#{user_id} and achievement_id=#{achievementId}")
    int isBaseUser(int user_id,int achievementId);



//sna version 2 who to follow
@Select("SELECT  * from sm_users order by id desc limit 10")
@Results(
        {
                @Result(property = "id",column = "id"),
                @Result(property = "uuid",column = "user_uuid"),
                @Result(property = "fullName",column = "full_name"),
                @Result(property = "profile",column = "image"),
                @Result(property = "gender",column = "gender"),
                @Result(property = "bio",column = "bio"),
                @Result(property = "email",column = "email"),
                @Result(property = "joinDate",column = "create_date"),
                @Result(property = "DateOfBirth",column = "date_of_birth"),
                @Result(property = "totalFollower",column = "id",one = @One (select = "countFollowing")),
                @Result(property = "totalFollowing",column = "id",one = @One (select = "countFollower")),
                @Result(property = "achievement",column = "id",many = @Many (select = "getAchievementByUserId")),
        })
List<UserInfoPayload> getWhoToFolow();







}

