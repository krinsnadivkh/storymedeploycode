package com.kshrd.storymeapi.repository.todo_list;

import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;
import com.kshrd.storymeapi.dto.request.todo_list.TodoListRequest;
import com.kshrd.storymeapi.model.todo_list.Todo;
import com.kshrd.storymeapi.model.todo_list.TodoList;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
@Repository
public interface TodoListRepository {


    @Select("INSERT INTO sm_todo_lists (uuid, todo_date, is_public, crate_date, user_id) VALUES " +
            "(#{td.uuid},#{td.todoDate},#{td.isPublish},#{td.createDate},#{td.userId}) returning id")
    int createTodoList(@Param("td")TodoList todoList);
//
//    private int id;
//    private Time todoTime;
//    private String todoText;
//    boolean isComplete;

//    @Insert("INSERT INTO sm_todo (todo_time, todo_text, iscomplete, todolist_id) " +
//            "VALUES (#{td.todoTime},#{td.todoText},#{td.isComplete})")
//    int createTodo(@Param("td") Todo todo);
//

    @Insert("INSERT INTO sm_todo ( uuid,todo_time,action, iscomplete, todolist_id) " +
            "VALUES (#{td.uuid},#{td.todoTime},#{td.action},#{td.isComplete},#{td.todolistId})")
    int createTodo(@Param("td") Todo todo);

    @Select("SELECT * from sm_todo_lists where id=#{id}")
    Todo findTodoListById(int id);

    @Select("SELECT * from sm_todo_lists where uuid=#{todoListUuid}")
    Todo findTodoListByUuid(String todoListUuid);

    @Update("UPDATE sm_todo_lists set is_public= #{privacy} where uuid=#{uuid}")
    int updatePrivacyTodoList(String  uuid,boolean privacy);


    @Select("SELECT * from sm_todo where uuid=#{uuid}")
    Todo findTodoByUuid (String uuid);

    @Delete("DELETE from sm_todo where uuid=#{uuid}")
    int deleteTodoByUuid(String  uuid);

    @Update("UPDATE sm_todo set iscomplete= #{status} where  uuid=#{todoUuid}")
    int updateComplete(String todoUuid,boolean status);

    @Select("SELECT iscomplete from sm_todo where uuid=#{todoUuid}")
    boolean isComplete (String todoUuid);

    @Delete("DELETE from sm_todo_lists where uuid=#{uuid}")
    int deleteTodoListByUuid(String  uuid);


    @Update("UPDATE sm_todo_lists set todo_date = #{td.todoDate} where uuid=#{td.uuid}")
    int updateTodoList (@Param("td") TodoList todoList);


//    v2

//        get all todo
    @Select("SELECT * from  sm_todo where todolist_id =#{todoListId}")
    @Results(
            {
                    @Result(property = "todoTime",column = "todo_time"),
                    @Result(property = "todolistId",column = "todolist_id"),
                    @Result(property = "todoDate",column = "todo_date"),
                    @Result(property = "uuid",column = "uuid"),
                    @Result(property = "uuid",column = "uuid"),
            }
    )
    List<Todo> getTodoByTodoListId(int todoListId);
//    End get all todo

    @Select("SELECT * from sm_todo_lists\n" +
            "where user_id=#{userID} order by id desc limit #{limit} offset #{offset}")

    @Results(
            {
                    @Result(property = "todo",column = "id",many = @Many (select = "getTodoByTodoListId")),
                    @Result(property = "isPublish",column = "is_public"),
                    @Result(property = "createDate",column = "crate_date"),
                    @Result(property = "todoDate",column = "todo_date"),
                    @Result(property = "userId",column = "user_id"),
                    @Result(property = "uuid",column = "uuid"),
                    @Result(property = "id",column = "id"),
            }
    )
    List<GetAllTodoListPayload> getAllTodolistByUserUuid(int userID, int limit,int offset);



    @Select("SELECT * from sm_todo_lists\n" +
            "where id=#{id}")

    @Results(
            {
                    @Result(property = "todo",column = "id",many = @Many (select = "getTodoByTodoListId")),
                    @Result(property = "isPublish",column = "is_public"),
                    @Result(property = "createDate",column = "crate_date"),
                    @Result(property = "todoDate",column = "todo_date"),
                    @Result(property = "userId",column = "user_id"),
                    @Result(property = "uuid",column = "uuid"),
                    @Result(property = "id",column = "id"),
            }
    )
  GetAllTodoListPayload getTodolistById(int id);





}
