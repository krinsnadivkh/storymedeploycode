package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**********************************************************************
 * Original Author: StoryMe API Team
 * Created Date: 30/05/2022
 * Development Group: Story Me
 * Description: UserPayload Class
 **********************************************************************/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPayload {
    private String id;
    private String full_name;
    private String email;
    private String status;
    private List<String> roles;
    private String token;

    private String profile;
    private String userUuid;
    private String gender;
    private String bio;
    private String dob;
}
