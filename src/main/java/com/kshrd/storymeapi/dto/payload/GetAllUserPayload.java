package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetAllUserPayload {
//    private String uuid;
//    private String fullName;
//    private String profile;
//    private int  follower;
//    private int  following;

    private String id;
    private String fullName;
    private String email;
    private String status;
    private String profile;
    private String userUuid;
    private String gender;
    private String bio;
    private String dob;
        private int  follower;
    private int  following;

}
