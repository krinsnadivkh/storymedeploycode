package com.kshrd.storymeapi.dto.payload;

import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateStoryPayload {
    private String title;
    private String postUuid;
    boolean isPublis;
    private int storytypeId;
    private String userUuid;
    private int emotionID;
    private String storyType;
    private String emotion;
    List<Paragraphs> paragraphs = new ArrayList<>();
    List<Comments> comments = new ArrayList<>();
}
