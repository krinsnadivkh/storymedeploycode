package com.kshrd.storymeapi.dto.payload;

import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import com.kshrd.storymeapi.model.users.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecentpostPayload {
    private int id;
    RecentUser users;
    private String storyUuid;
    private String title;
    private String storyType;
    private String emotion;
    private boolean ispublic;
    private int likeCount;
    private int veiwCount;
    private int commentCount;
    private String createDate;
    List<Paragraphs> paragraphs = new ArrayList<>();
    List<Comments> comments = new ArrayList<>();
}
