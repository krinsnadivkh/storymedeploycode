package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FollowingResponePayload {
        private String id;
        private String uuid;
        private String fullName;
        private String profile;
        private int totalFollower;
}
