package com.kshrd.storymeapi.dto.payload;


import com.kshrd.storymeapi.model.todo_list.Todo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetAllTodoListPayload {
    private int id;
    private String uuid;
    boolean isPublish;
    private LocalDateTime createDate;
    private LocalDate todoDate;
    private int userId;
    private List<Todo> todo ;

}
