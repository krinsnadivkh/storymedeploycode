package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserInfoPayload {
    private int id;
    private String uuid;
    private String fullName;
    private String gender;
    private String bio;
    private String profile;
    private int totalFollowing;
    private int totalFollower;
    private String email;
//    private LocalDateTime DateOfBirth;
    private String DateOfBirth;
    private LocalDateTime joinDate;
    private List<String> achievement;
}
