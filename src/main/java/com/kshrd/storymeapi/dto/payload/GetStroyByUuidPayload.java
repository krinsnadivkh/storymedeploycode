package com.kshrd.storymeapi.dto.payload;

import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetStroyByUuidPayload {
    private int id;
    private String fullName;
    private String userProfile;
    private String userUuid;
    private String storyType;
    private String emotion;
    private String createDate;
    private String storyUuid;
    private String title;
    private boolean ispublic;
    private int likeCount;
    private int veiwCount;
    private int commentCount;
    List<Paragraphs> paragraphs = new ArrayList<>();
    List<Comments> comments = new ArrayList<>();
}
