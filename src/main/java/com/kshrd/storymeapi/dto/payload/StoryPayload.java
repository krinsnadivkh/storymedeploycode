package com.kshrd.storymeapi.dto.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StoryPayload {
    private String title;
    private boolean isPublic;
}
