package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecentUser {
    private int id;
    private String user_uuid;
    private String full_name;
    private String image;

}
