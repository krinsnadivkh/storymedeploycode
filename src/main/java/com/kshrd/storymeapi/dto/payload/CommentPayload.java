package com.kshrd.storymeapi.dto.payload;

import com.kshrd.storymeapi.model.users.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentPayload {
    private int id;
    private String commentUuid;
    private String content;
    private LocalDateTime createDate;
    private String userName;
    private String profile;

}
