package com.kshrd.storymeapi.dto.payload;

import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public class MemorizeStoryPayload {
        private int id;
        private String userProfile;
        private String fullName;
        private String userUuid;
        private String storyUuid;
        private String title;
        private String storyType;
        private String emotion;
        private boolean ispublic;
        private int likeCount;
        private int veiwCount;
        private int commentCount;
        private String createDate;
        List<Paragraphs> paragraphs = new ArrayList<>();
        List<Comments> comments = new ArrayList<>();
    }



