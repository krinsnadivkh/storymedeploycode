package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParagraphsPayload {
    private int id;
    private String paragraph;
    private String image;
    private int storyId;
}
