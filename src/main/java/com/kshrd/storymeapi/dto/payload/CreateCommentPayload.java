package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateCommentPayload {
    private String content;
    private LocalDateTime createDate;
    private String userName;
    private String profile;
}
