package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchUserPayload {
    private int id;
    private String uuid;
    private String name;
    private String bio;
    private String profile;
    private int followerCount;
}
