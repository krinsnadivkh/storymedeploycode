package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetAllJourneyPayload {
    private int id;
    private String uuid;
    private String title;
    private String  description;
    private String image;
    private LocalDateTime createDate;
    private boolean isPublic;

}
