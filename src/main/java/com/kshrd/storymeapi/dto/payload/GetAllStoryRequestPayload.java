package com.kshrd.storymeapi.dto.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetAllStoryRequestPayload {
    private String uuid;
    private String createDate;
    private String fullName;
    private String storyRequest;
    private String profile;
    private String storyUuid;
}
