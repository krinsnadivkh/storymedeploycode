package com.kshrd.storymeapi.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminPayload {
    private String uuid;
    private String fullName;
    private String email;
    private String gender;
    private String profile;
    private String  dateOfBirth;

}
