package com.kshrd.storymeapi.dto.request.sotry_post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateStoryPrivacyRequest {
    private String uuid;
    private boolean isPublic;

}
