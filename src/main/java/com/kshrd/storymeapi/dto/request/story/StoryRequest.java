package com.kshrd.storymeapi.dto.request.story;

import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StoryRequest {

    @NotBlank(message = "title cannot Be blank")
    private String title;

    boolean isPublis;
    @NotBlank(message = "storyType uuid cannot Be blank")
    private String storyType;
    @NotBlank(message = "User Uuid uuid cannot Be blank")
    private String userUuid;

    private int emotionID;
}
