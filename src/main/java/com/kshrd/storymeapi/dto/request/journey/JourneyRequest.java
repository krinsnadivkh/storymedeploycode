package com.kshrd.storymeapi.dto.request.journey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;

//
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JourneyRequest {
        @NotBlank(message = "Title cannot Be blank")
        private String title;
        @NotBlank(message = "Description cannot Be blank")
        private String description;
        boolean isPublis;
        private String image;

        private int userId;


}
