package com.kshrd.storymeapi.dto.request.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentRequest {
    @NotBlank(message = "Comment cannot Be blank")
    private String content;
    private int storyId;
//    private int userId;
    private String userUuid;
}


