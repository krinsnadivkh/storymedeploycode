package com.kshrd.storymeapi.dto.request.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserForgotPasswordRequest {
    @NotBlank(message = "email can't be blank")
    private  String Email;
}
