package com.kshrd.storymeapi.dto.request.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FollowAndUnfollowRequest {
   private int otherUserId;
}
