package com.kshrd.storymeapi.dto.request.todo_list;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateTodoListRequest {

    @NotBlank(message = "Todo List cannot Be blank")
    String todoListUuid;
    LocalDateTime todoDate;
}
