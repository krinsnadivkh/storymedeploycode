package com.kshrd.storymeapi.dto.request.todo_list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateCommpleteStatusRequest {
    private String todoUuid;
}
