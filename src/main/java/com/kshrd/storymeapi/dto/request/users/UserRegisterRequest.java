package com.kshrd.storymeapi.dto.request.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterRequest {
    @NotBlank(message = "full name can't be blank")
    private String full_name;
    @NotBlank(message = "email can't be blank")
    @Email
    private String email;
    @NotBlank(message = "Password can't be blank")
    @Size(min = 6, max = 10, message
            = "password must be between 6 and 10 characters")
    private String password;
    @NotBlank(message = "Gender can't be blank")
    private String gender;
    //    @NonNull(message = "Date of Birth can't be blank")
    private String date_of_birth;

}
