package com.kshrd.storymeapi.dto.request.sotry_post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StoryPostRequest {
    @NotBlank(message = "Title cannot Be blank")
    private String title;


}
