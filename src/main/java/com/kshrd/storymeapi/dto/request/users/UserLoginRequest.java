package com.kshrd.storymeapi.dto.request.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**********************************************************************
 * Original Author: Try Krisna
 * Created Date: 30/05/2022
 * Development Group: StoryMe
 * Description: Author Request Class
 **********************************************************************/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginRequest {
    @NotBlank(message = "email can't be blank")
    @Email
    private String email;
    @NotBlank(message = "Password can't be blank")
    @Size(min = 6, max = 10, message
            = "password must be between 6 and 10 characters")
    private String password;

}
