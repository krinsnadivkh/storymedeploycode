package com.kshrd.storymeapi.dto.request.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateStoryRequest {
    @NotBlank(message = "cannot Be blank")
    private String postUuid;
    @NotBlank(message = "Title cannot Be blank")
    private String title;

    boolean isPublis;
    @NotBlank(message = "cannot Be blank")
    private String storyType;
//    @NotBlank(message = "cannot Be blank")
//    private String userUuid;

    private int emotionID;
}
