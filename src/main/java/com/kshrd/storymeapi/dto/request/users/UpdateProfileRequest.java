package com.kshrd.storymeapi.dto.request.users;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProfileRequest {

   @NotBlank(message = "User Uuid cannot Be blank")
   private String userUuid;
   @NotBlank(message = "Full Name Uuid cannot Be blank")
   private String fullName;
   @NotBlank(message = "Gender Uuid cannot Be blank")
   private String gender;
   @NotBlank(message = "Email Uuid cannot Be blank")
   @Email
   private String email;
   private String image;
   @NotBlank(message = "Bio Uuid cannot Be blank")
   private String bio;
   private LocalDateTime dateOfBirth;
}
