package com.kshrd.storymeapi.dto.request.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParagraphsRequest {
    @NotBlank(message = "Paragraphs cannot Be blank")
    private String paragraphs;
    private String image;
}
