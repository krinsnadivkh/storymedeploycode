package com.kshrd.storymeapi.dto.request.todo_list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TodoListRequest {

    private LocalDateTime createDate;
    private LocalDateTime todoDate;
    boolean isPublish;
    @NotBlank(message = "UserUuid  cannot Be blank")
    private String userUuid;

}
