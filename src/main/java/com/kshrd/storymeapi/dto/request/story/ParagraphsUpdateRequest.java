package com.kshrd.storymeapi.dto.request.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParagraphsUpdateRequest {
//    @NotBlank(message = "uuid cannot Be blank")
//    private String uuid;
    private int id ;
    @NotBlank(message = "paragraph uuid cannot Be blank")
    private String paragraphs;
    private String image;

}
