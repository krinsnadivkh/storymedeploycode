package com.kshrd.storymeapi.dto.request.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResetPasswordRequest {

    @Size(min = 6, max = 10, message
            = "password must be between 6 and 10 characters")
    private String password;

    @Size(min = 6, max = 10, message
            = "password must be between 6 and 10 characters")
    private String comfrimePassword;
//    private String Token;

}
