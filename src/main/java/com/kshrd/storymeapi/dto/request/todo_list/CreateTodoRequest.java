package com.kshrd.storymeapi.dto.request.todo_list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateTodoRequest {
    @NotBlank(message = "To do time Uuid cannot Be blank")
    private String time;
    @NotBlank(message = "Action cannot Be blank")
    private String action;

}
