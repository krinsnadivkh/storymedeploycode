package com.kshrd.storymeapi.dto.request.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class IsLikeRequest {
    private int userId;
    private int storyId;
}
