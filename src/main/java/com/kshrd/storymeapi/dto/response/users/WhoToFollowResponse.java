package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;

import java.util.Date;
import java.util.List;

public class WhoToFollowResponse  extends BaseResponse {
    public WhoToFollowResponse(int code, String message, Date requestedTime, List<UserInfoPayload> whoToFollow) {
        super(code, message, requestedTime, whoToFollow);
    }
}
