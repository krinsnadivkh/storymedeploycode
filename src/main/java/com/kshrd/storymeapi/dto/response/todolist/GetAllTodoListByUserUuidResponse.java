package com.kshrd.storymeapi.dto.response.todolist;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;

import java.util.Date;
import java.util.List;

public class GetAllTodoListByUserUuidResponse extends BaseResponse {
    public GetAllTodoListByUserUuidResponse(int code, String message, Date requestedTime, List<GetAllTodoListPayload> getAllTodoListPayloads) {
        super(code, message, requestedTime, getAllTodoListPayloads);
    }
}
