package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.CommentPayload;

import java.util.Date;

public class CreateCommentResponse extends BaseResponse {
    public CreateCommentResponse(int code, String message, Date requestedTime, CommentPayload commentPayload) {
        super(code, message, requestedTime, commentPayload);
    }
}
