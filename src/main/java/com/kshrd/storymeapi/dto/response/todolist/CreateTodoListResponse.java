package com.kshrd.storymeapi.dto.response.todolist;


import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllTodoListPayload;
import com.kshrd.storymeapi.dto.payload.StoryPayload;
import lombok.Data;
import java.util.Date;

@Data
public class CreateTodoListResponse extends BaseResponse {
    public CreateTodoListResponse(int code, String message, Date requestedTime, GetAllTodoListPayload getAllTodoListPayload) {
        super(code, message, requestedTime, getAllTodoListPayload);
    }
}
