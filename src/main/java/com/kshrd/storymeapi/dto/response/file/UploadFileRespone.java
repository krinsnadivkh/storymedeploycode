package com.kshrd.storymeapi.dto.response.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UploadFileRespone {
    private String fileName;
    private String fileUrl;
    private String fileType;
    private long size;
}
