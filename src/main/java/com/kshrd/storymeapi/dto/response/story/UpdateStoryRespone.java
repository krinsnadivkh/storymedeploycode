package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllPostByUserIDPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;


public class UpdateStoryRespone extends BaseResponse {
    public UpdateStoryRespone(int code, String message, Date requestedTime,UpdateResponse updateResponse) {
        super(code, message, requestedTime, updateResponse);
    }
}
