package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.FollowingResponePayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;


@Data
public class CountFollowerResponse extends BaseResponse {
    public CountFollowerResponse(int code, String message, Date requestedTime, int totalFollower) {
        super(code, message, requestedTime, totalFollower);
    }
//    int totalFollower;
}
