package com.kshrd.storymeapi.dto.response.users;


import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.UserPayload;
import lombok.Data;

import java.util.Date;


@Data
public class UserLoginResponse extends BaseResponse<UserPayload> {
    public UserLoginResponse(int code, String message, Date requestedTime, UserPayload payload) {
        super(code, message, requestedTime, payload);
    }

    public UserLoginResponse(int code, String ok, Date requestTime, String id, String username, String email) {
    }
}
