package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllPostByUserIDPayload;
import com.kshrd.storymeapi.dto.payload.StoryPayload;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//@AllArgsConstructor
//@NoArgsConstructor
@Data
public class GetAllPostByUserIDRespone extends BaseResponse {

    public GetAllPostByUserIDRespone(int code, String message, Date requestedTime,List<GetAllPostByUserIDPayload> response) {
        super(code, message, requestedTime, response);
    }


}
