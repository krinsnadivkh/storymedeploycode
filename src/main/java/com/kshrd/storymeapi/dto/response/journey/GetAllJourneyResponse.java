package com.kshrd.storymeapi.dto.response.journey;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;

import java.util.Date;
import java.util.List;

public class GetAllJourneyResponse extends BaseResponse {

    public GetAllJourneyResponse(int code, String message, Date requestedTime, List<GetAllJourneyPayload> getAllJourneyPayload) {
        super(code, message, requestedTime, getAllJourneyPayload);
    }
}
