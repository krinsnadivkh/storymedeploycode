package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.EmotionPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;


public class AllEmotionResponse extends BaseResponse {
    public AllEmotionResponse(int code, String message, Date requestedTime, List<EmotionPayload> emotionPayloads) {
        super(code, message, requestedTime, emotionPayloads);
    }
}
