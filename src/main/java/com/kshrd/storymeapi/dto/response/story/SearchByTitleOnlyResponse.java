package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.SearchStoryPostPayload;

import java.util.Date;
import java.util.List;

public class SearchByTitleOnlyResponse extends BaseResponse {

    public SearchByTitleOnlyResponse(int code, String message, Date requestedTime, List<SearchStoryPostPayload> searchStoryPostPayloads) {
        super(code, message, requestedTime, searchStoryPostPayloads);
    }
}
