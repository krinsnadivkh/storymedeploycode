package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllPostByUserIDPayload;
import com.kshrd.storymeapi.dto.payload.GetStroyByUuidPayload;
import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class GetStroyByUuidRespone extends BaseResponse {

    public GetStroyByUuidRespone(int code, String message, Date requestedTime, GetStroyByUuidPayload getStroyByUuidPayload) {
        super(code, message, requestedTime, getStroyByUuidPayload);
    }

}
