package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetStroyByUuidPayload;

import java.util.Date;

public class createStoryRespone extends BaseResponse{
    public createStoryRespone(int code, String message, Date requestedTime, GetStroyByUuidPayload getStroyByUuidPayload) {
        super(code, message, requestedTime, getStroyByUuidPayload);
    }
}
