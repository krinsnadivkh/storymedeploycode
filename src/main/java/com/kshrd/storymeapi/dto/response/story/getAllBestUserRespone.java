package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllPostByUserIDPayload;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;

import java.util.Date;
import java.util.List;

public class getAllBestUserRespone extends BaseResponse {
    public getAllBestUserRespone(int code, String message, Date requestedTime, List<GetAllPostByUserIDPayload> users) {
        super(code, message, requestedTime, users);
    }
}
