package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.SearchUserPayload;

import java.util.Date;
import java.util.List;

public class SearchUserByNameResponse  extends BaseResponse {
    public SearchUserByNameResponse(int code, String message, Date requestedTime, List<SearchUserPayload>searchUser)  {
        super(code, message, requestedTime, searchUser);
    }
}
