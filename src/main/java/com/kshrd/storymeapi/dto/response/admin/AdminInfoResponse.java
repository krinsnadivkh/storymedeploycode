package com.kshrd.storymeapi.dto.response.admin;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.AdminPayload;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;

import java.util.Date;
import java.util.List;

public class AdminInfoResponse extends BaseResponse {
    public AdminInfoResponse(int code, String message, Date requestedTime, AdminPayload adminPayload) {
        super(code, message, requestedTime, adminPayload);
    }
}
