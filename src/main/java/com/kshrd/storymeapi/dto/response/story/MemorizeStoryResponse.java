package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.MemorizeStoryPayload;

import java.util.Date;
import java.util.List;

public class MemorizeStoryResponse extends BaseResponse {

    public MemorizeStoryResponse(int code, String message, Date requestedTime, List<MemorizeStoryPayload> memorizeStoryPayloads) {
        super(code, message, requestedTime, memorizeStoryPayloads);
    }
}
