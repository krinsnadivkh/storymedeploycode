package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetStroyByUuidPayload;
import com.kshrd.storymeapi.dto.payload.RecentpostPayload;

import java.util.Date;
import java.util.List;

public class RecentPostResponse extends BaseResponse {

    public RecentPostResponse(int code, String message, Date requestedTime, List<RecentpostPayload> recentpostPayload) {
        super(code, message, requestedTime, recentpostPayload);
    }

}
