package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;

import java.util.Date;
import java.util.List;

public class bestUserResponse extends BaseResponse {
    public bestUserResponse(int code, String message, Date requestedTime, List<UserInfoPayload> userInfo) {
        super(code, message, requestedTime, userInfo);
    }
}
