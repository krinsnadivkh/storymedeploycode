package com.kshrd.storymeapi.dto.response.journey;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;

import java.util.Date;

public class CreateJouneyResponse extends BaseResponse {

    public CreateJouneyResponse(int code, String message, Date requestedTime, GetAllJourneyPayload getAllJourneyPayload) {
        super(code, message, requestedTime, getAllJourneyPayload);
    }
}
