package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.request.users.UserRegisterRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Date;

//@AllArgsConstructor
//@NoArgsConstructor
@Data
public class UserRegisterResponse extends BaseResponse<UserRegisterRequest> {
    public UserRegisterResponse(int code, String message, Date requestTime,UserRegisterRequest payload) {
        super(code, message, requestTime, payload);
    }
}
