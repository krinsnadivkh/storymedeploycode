package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.StoryPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StoryRespones extends BaseResponse {
    private String title;
    private boolean isPublic;
    public StoryRespones(int code, String message, Date requestedTime, StoryPayload storyPayload) {
        super(code, message, requestedTime, storyPayload);
    }


    public StoryRespones(int code, String fetch_story_done, Date requestTime, StoryRespones allStory) {
    }
}
