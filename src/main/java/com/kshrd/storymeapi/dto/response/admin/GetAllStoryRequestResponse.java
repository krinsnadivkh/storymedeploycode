package com.kshrd.storymeapi.dto.response.admin;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllStoryRequestPayload;

import java.util.Date;
import java.util.List;

public class GetAllStoryRequestResponse extends BaseResponse {
    public GetAllStoryRequestResponse(int code, String message, Date requestedTime, List<GetAllStoryRequestPayload> getAllStoryRequestPayloads) {
        super(code, message, requestedTime, getAllStoryRequestPayloads);
    }
}
