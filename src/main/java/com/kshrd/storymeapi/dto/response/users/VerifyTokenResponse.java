package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.UserPayload;
import com.kshrd.storymeapi.dto.payload.VerifyTokenPayload;

import java.util.Date;

public class VerifyTokenResponse extends BaseResponse {

    public VerifyTokenResponse(int code, String message, Date requestedTime, VerifyTokenPayload verifyTokenPayload) {
        super(code, message, requestedTime, verifyTokenPayload);
    }

}
