package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;

import java.util.Date;

public class IsLikeResponse extends BaseResponse {
    public IsLikeResponse(int code, String message, Date requestedTime, boolean isLike) {
        super(code, message, requestedTime, isLike);
    }
}
