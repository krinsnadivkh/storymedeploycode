package com.kshrd.storymeapi.dto.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValidModel {
    private String defaultMessage;
    private String field;
}
