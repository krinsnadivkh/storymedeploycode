package com.kshrd.storymeapi.dto.response.admin;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;

import java.util.Date;
import java.util.List;

public class TotalUserRespone extends BaseResponse {
    public TotalUserRespone(int code, String message, Date requestedTime, int totalUser) {
        super(code, message, requestedTime, totalUser);
    }

}
