package com.kshrd.storymeapi.dto.response.users;


import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.UserInfoPayload;
import com.kshrd.storymeapi.model.users.Users;

import java.util.Date;

public class UserInfoResponse extends BaseResponse {
    public UserInfoResponse(int code, String message, Date requestedTime, UserInfoPayload users) {
        super(code, message, requestedTime, users);
    }
}
