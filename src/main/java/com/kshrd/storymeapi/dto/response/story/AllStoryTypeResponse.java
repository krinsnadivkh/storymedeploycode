package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.dto.BaseResponse;

import java.util.Date;
import java.util.List;

public class AllStoryTypeResponse extends BaseResponse {
    public AllStoryTypeResponse(int code, String message, Date requestedTime, List<String> allType) {
        super(code, message, requestedTime, allType);
    }
}
