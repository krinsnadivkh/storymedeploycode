package com.kshrd.storymeapi.dto.response.journey;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllJourneyPayload;
import com.kshrd.storymeapi.dto.payload.StoryPayload;
import lombok.AllArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@AllArgsConstructor
@NotNull
public class JourneyResponse extends BaseResponse {
    public JourneyResponse(int code, String message, Date requestedTime, GetAllJourneyPayload getAllJourneyPayload) {
        super(code, message, requestedTime, getAllJourneyPayload);
    }

}
