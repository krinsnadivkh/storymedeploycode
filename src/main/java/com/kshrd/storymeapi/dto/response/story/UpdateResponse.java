package com.kshrd.storymeapi.dto.response.story;

import com.kshrd.storymeapi.model.story.Comments;
import com.kshrd.storymeapi.model.story.Paragraphs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateResponse {
    private String title;
    private String postUuid;
    boolean isPublis;
    private String storyType;
    private String emotion;
    List<Paragraphs> paragraphs = new ArrayList<>();
    List<Comments> comments = new ArrayList<>();
}
