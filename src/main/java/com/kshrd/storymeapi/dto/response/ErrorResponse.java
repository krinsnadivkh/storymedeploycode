package com.kshrd.storymeapi.dto.response;



import com.kshrd.storymeapi.dto.BaseErrorResponse;
import com.kshrd.storymeapi.dto.payload.ErrorPayload;

import java.util.Date;
import java.util.List;

public class ErrorResponse extends BaseErrorResponse<List<ErrorPayload>> {
    public ErrorResponse(int code, String message, Date requestTime, List<ErrorPayload> error) {
        super(code, message, requestTime, error);
    }
}
