package com.kshrd.storymeapi.dto.response;

import com.kshrd.storymeapi.dto.BaseValidResponse;

import java.time.LocalDateTime;
import java.util.Map;

public class ValidResponse  extends BaseValidResponse {
    public ValidResponse(int code, String message, LocalDateTime requestTime, Map errors) {
        super(code, message, requestTime, errors);
    }
}
