package com.kshrd.storymeapi.dto.response.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLogingResponse {
    private int id ;
    private String full_name;
    private List<String> roles;
    private String token;

}
