package com.kshrd.storymeapi.dto.response.admin;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.GetAllUserPayload;
import com.kshrd.storymeapi.dto.payload.StoryPayload;

import java.util.Date;
import java.util.List;


public class GetAllUserResponse extends BaseResponse {
    public GetAllUserResponse(int code, String message, Date requestedTime, List<GetAllUserPayload> getAllUserPayloads) {
        super(code, message, requestedTime, getAllUserPayloads);
    }
}
