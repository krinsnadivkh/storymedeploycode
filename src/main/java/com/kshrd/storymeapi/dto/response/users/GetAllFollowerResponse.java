package com.kshrd.storymeapi.dto.response.users;

import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.dto.payload.FollowingResponePayload;
import lombok.Data;

import java.util.Date;
import java.util.List;

//@AllArgsConstructor
//@NoArgsConstructor
@Data
public class GetAllFollowerResponse extends BaseResponse {
    public GetAllFollowerResponse(int code, String message, Date requestedTime, List<FollowingResponePayload>followerPayloads) {
        super(code, message, requestedTime, followerPayloads);
    }

}
