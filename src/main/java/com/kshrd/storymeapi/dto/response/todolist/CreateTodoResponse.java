package com.kshrd.storymeapi.dto.response.todolist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
@AllArgsConstructor
@NoArgsConstructor
@Data

public class CreateTodoResponse {
    private Time time;
    private String todoText;
    boolean status;

}
