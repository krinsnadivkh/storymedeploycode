package com.kshrd.storymeapi.dto.response.todolist;


import com.kshrd.storymeapi.dto.BaseResponse;
import com.kshrd.storymeapi.model.todo_list.Todo;

import java.util.Date;
import java.util.List;

public class TodoMyResponse extends BaseResponse {
    public TodoMyResponse(int code, String message, Date requestedTime, List<Todo> myTodo) {
        super(code, message, requestedTime, myTodo);
    }
}
