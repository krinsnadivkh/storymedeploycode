package com.kshrd.storymeapi.utilities;

import lombok.Data;

@Data
public class CustomMessage {
    public static final String OK = "OK";
    public static final String BAD_REQUEST = "Bad Request";
    private static final String SUCCESSFULLY_RETRIEVED = "retrieved successfully";
    public static final String SUCCESSFULLY_UPDATED = "successfully updated";
    public static final String NOT_FOUND = "Not Found";
    public static final String VALIDATION_ERROR = "Validation Error";
    public static final String SUCCESSFULLY_CREATED = "created successfully";
    public static final String SUCCESSFULLY_DELETED = "deleted successfully";
    public static final String SUCCESSFULLY_SUMMITED = "summited successfully";

//    sna
public static final String USER_NOT_FOUND = "User not found";
public static final String SUCCESSFULLY_LOGIN = "Successfully Login";
}
