package com.kshrd.storymeapi.utilities.customfuntion;

import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.Period;

//@AllArgsConstructor
//@NoArgsConstructor
@Data
public class CustomFuntion {
    public static String getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(principal);
        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
            System.out.println("if"+username);
        } else {
            username = principal.toString();
            System.out.println("else"+username);
        }
        return username;
    }

    public static Period findDifference(LocalDate start_date,
                                        LocalDate end_date)
    {

        // find the period between
        // the start and end date
        Period diff
                = Period
                .between(start_date,
                        end_date);

        // Print the date difference
        // in years, months, and days
        System.out.print(
                "Difference "
                        + "between two dates is: ");

        // Print the result
//        System.out.printf(
//                "%d years, %d months"
//                        + " and %d days ",
//                diff.getYears(),
//                diff.getMonths(),
//                diff.getDays());
////        LocalDate s = diff;
        return diff;
    }











}
