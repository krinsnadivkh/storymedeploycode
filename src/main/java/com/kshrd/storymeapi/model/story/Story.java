package com.kshrd.storymeapi.model.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Story {
    private int id;
//    private UUID uuid;
    private String uuid;
    private String title;
    private boolean isPublic;
    private int likeCount;
//    private int userID;
    private int veiwCount;
    private int commentCount;
    private int storyTypeID;
    private int emotionID;
    private String createDate;
    private String ownerId;
    private int userId;
    List<Paragraphs> paragraphs = new ArrayList<>();

}
