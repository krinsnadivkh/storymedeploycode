package com.kshrd.storymeapi.model.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.objenesis.instantiator.perc.PercInstantiator;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Comments {
    private int id;
    private UUID uuid;
    private String content;
    private LocalDateTime createDate;
    private int storyId;
    private int userId;
//    private String userUuid;
}
