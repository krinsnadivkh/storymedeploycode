package com.kshrd.storymeapi.model.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequestStory {
private int userId;
  private   String uuid;
    private String storyType;
    private boolean isAllow;

}


