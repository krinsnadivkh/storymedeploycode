package com.kshrd.storymeapi.model.story;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Paragraphs {
    private int id;
//    private UUID uuid;
//    sna
    private String uuid;
    private String paragraph;
    private String image;
    private int storyId;
}
