package com.kshrd.storymeapi.model.journey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Journey {
    private int id;
//    private UUID uuid;
    private String uuid ;
    private String title;
    private String  description;
    private String image;
    private LocalDateTime createDate;
    private boolean isPublic;
    private int userId;
}
