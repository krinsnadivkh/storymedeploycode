package com.kshrd.storymeapi.model.todo_list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TodoList {
    private int id;
    private String uuid;
    private String todo;
    boolean isPublish;
    private boolean status;
    private LocalDateTime createDate;
    private LocalDateTime todoDate;
    private String UserUuid;
    private int userId;
}
