package com.kshrd.storymeapi.model.todo_list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Todo {
    private int id;
    private String uuid;
    private String todoTime;
    private String action;
    boolean isComplete;
    private int todolistId;



}
