package com.kshrd.storymeapi.model.users;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {
    private int id;
    private String full_name;
    private String email;
    private String status;
    private List<String> roles;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdAt;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updatedAt;
}
