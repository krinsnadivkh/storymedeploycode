package com.kshrd.storymeapi.model.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthUser {
    private int id ;
    private String email;
    private String full_name ;
    private String password;
    private String status;
    private List<String> roles;

    private String profile;
    private String userUuid;
    private String gender;
    private String bio;
    private String dob;
}
