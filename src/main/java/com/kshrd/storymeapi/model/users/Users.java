package com.kshrd.storymeapi.model.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class Users {
    private int id;

    private UUID uuid;
    private String full_name;
    private String gender;

    private String email;
    private String password;
    private String date_of_birth;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private String image;
    private List<String> roles;
    private boolean status;
    private String resetPasswordToken;

}
