/*
 Navicat Premium Data Transfer

 Source Server         : storyme
 Source Server Type    : PostgreSQL
 Source Server Version : 140003
 Source Host           : 8.219.139.67:5423
 Source Catalog        : story_me
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140003
 File Encoding         : 65001

 Date: 30/06/2022 17:22:58
*/


-- ----------------------------
-- Sequence structure for sm_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_comment_id_seq";
CREATE SEQUENCE "public"."sm_comment_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_emotions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_emotions_id_seq";
CREATE SEQUENCE "public"."sm_emotions_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_following_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_following_id_seq";
CREATE SEQUENCE "public"."sm_following_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_journeys_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_journeys_id_seq";
CREATE SEQUENCE "public"."sm_journeys_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_likes_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_likes_id_seq";
CREATE SEQUENCE "public"."sm_likes_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_paragraphs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_paragraphs_id_seq";
CREATE SEQUENCE "public"."sm_paragraphs_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_role_id_seq";
CREATE SEQUENCE "public"."sm_role_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_story_posts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_story_posts_id_seq";
CREATE SEQUENCE "public"."sm_story_posts_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_story_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_story_type_id_seq";
CREATE SEQUENCE "public"."sm_story_type_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_todo_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_todo_id_seq";
CREATE SEQUENCE "public"."sm_todo_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_todo_lists_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_todo_lists_id_seq";
CREATE SEQUENCE "public"."sm_todo_lists_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_user_role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_user_role_id_seq";
CREATE SEQUENCE "public"."sm_user_role_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sm_users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sm_users_id_seq";
CREATE SEQUENCE "public"."sm_users_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for sm_comment
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_comment";
CREATE TABLE "public"."sm_comment" (
                                       "id" int4 NOT NULL DEFAULT nextval('sm_comment_id_seq'::regclass),
                                       "uuid" varchar(255) COLLATE "pg_catalog"."default",
                                       "content" varchar(255) COLLATE "pg_catalog"."default",
                                       "create_date" timestamp(6),
                                       "status" bool DEFAULT true,
                                       "story_id" int4 NOT NULL,
                                       "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_emotions
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_emotions";
CREATE TABLE "public"."sm_emotions" (
                                        "id" int4 NOT NULL DEFAULT nextval('sm_emotions_id_seq'::regclass),
                                        "uuid" varchar(255) COLLATE "pg_catalog"."default",
                                        "name" varchar(25) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_following
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_following";
CREATE TABLE "public"."sm_following" (
                                         "id" int4 NOT NULL DEFAULT nextval('sm_following_id_seq'::regclass),
                                         "following_id" int4 NOT NULL,
                                         "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_journeys
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_journeys";
CREATE TABLE "public"."sm_journeys" (
                                        "id" int4 NOT NULL DEFAULT nextval('sm_journeys_id_seq'::regclass),
                                        "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
                                        "title" varchar(150) COLLATE "pg_catalog"."default",
                                        "description" varchar(255) COLLATE "pg_catalog"."default",
                                        "image" varchar(255) COLLATE "pg_catalog"."default",
                                        "is_public" bool DEFAULT false,
                                        "create_date" timestamp(6),
                                        "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_likes
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_likes";
CREATE TABLE "public"."sm_likes" (
                                     "id" int4 NOT NULL DEFAULT nextval('sm_likes_id_seq'::regclass),
                                     "story_id" int4 NOT NULL,
                                     "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_paragraphs
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_paragraphs";
CREATE TABLE "public"."sm_paragraphs" (
                                          "id" int4 NOT NULL DEFAULT nextval('sm_paragraphs_id_seq'::regclass),
                                          "uuid" varchar COLLATE "pg_catalog"."default" NOT NULL,
                                          "paragraph" varchar(255) COLLATE "pg_catalog"."default",
                                          "image" varchar COLLATE "pg_catalog"."default",
                                          "story_id" int4
)
;

-- ----------------------------
-- Table structure for sm_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_roles";
CREATE TABLE "public"."sm_roles" (
                                     "id" int4 NOT NULL DEFAULT nextval('sm_role_id_seq'::regclass),
                                     "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_story_posts
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_story_posts";
CREATE TABLE "public"."sm_story_posts" (
                                           "id" int4 NOT NULL DEFAULT nextval('sm_story_posts_id_seq'::regclass),
                                           "uuid" varchar(255) COLLATE "pg_catalog"."default",
                                           "title" varchar(150) COLLATE "pg_catalog"."default",
                                           "is_public" bool NOT NULL DEFAULT true,
                                           "create_date" timestamp(6),
                                           "veiw_count" int4,
                                           "like_count" int4,
                                           "comment_count" int4,
                                           "story_type_id" int4 NOT NULL,
                                           "user_id" int4,
                                           "emotion_id" int4 NOT NULL,
                                           "owner_uuid" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for sm_story_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_story_type";
CREATE TABLE "public"."sm_story_type" (
                                          "id" int4 NOT NULL DEFAULT nextval('sm_story_type_id_seq'::regclass),
                                          "uuid" varchar(255) COLLATE "pg_catalog"."default",
                                          "story_type" varchar(15) COLLATE "pg_catalog"."default",
                                          "is_allow" bool NOT NULL DEFAULT true,
                                          "user_id" int4
)
;

-- ----------------------------
-- Table structure for sm_todo
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_todo";
CREATE TABLE "public"."sm_todo" (
                                    "id" int4 NOT NULL DEFAULT nextval('sm_todo_id_seq'::regclass),
                                    "todo_time" varchar COLLATE "pg_catalog"."default",
                                    "action" varchar(100) COLLATE "pg_catalog"."default",
                                    "iscomplete" varchar(255) COLLATE "pg_catalog"."default",
                                    "todolist_id" int4,
                                    "uuid" varchar COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for sm_todo_lists
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_todo_lists";
CREATE TABLE "public"."sm_todo_lists" (
                                          "id" int4 NOT NULL DEFAULT nextval('sm_todo_lists_id_seq'::regclass),
                                          "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
                                          "todo_date" timestamp(6),
                                          "is_public" bool DEFAULT false,
                                          "crate_date" timestamp(6),
                                          "user_id" int4
)
;

-- ----------------------------
-- Table structure for sm_user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_user_roles";
CREATE TABLE "public"."sm_user_roles" (
                                          "id" int4 NOT NULL DEFAULT nextval('sm_user_role_id_seq'::regclass),
                                          "role_id" int4 NOT NULL,
                                          "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for sm_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."sm_users";
CREATE TABLE "public"."sm_users" (
                                     "id" int4 NOT NULL DEFAULT nextval('sm_users_id_seq'::regclass),
                                     "user_uuid" varchar(255) COLLATE "pg_catalog"."default",
                                     "full_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
                                     "gender" varchar(6) COLLATE "pg_catalog"."default",
                                     "email" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
                                     "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
                                     "date_of_birth" timestamp(6),
                                     "image" varchar(255) COLLATE "pg_catalog"."default",
                                     "status" bool NOT NULL DEFAULT true,
                                     "create_date" timestamp(6),
                                     "update_date" timestamp(6),
                                     "reset_password_token" varchar COLLATE "pg_catalog"."default",
                                     "bio" varchar COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_comment_id_seq"
    OWNED BY "public"."sm_comment"."id";
SELECT setval('"public"."sm_comment_id_seq"', 19, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_emotions_id_seq"
    OWNED BY "public"."sm_emotions"."id";
SELECT setval('"public"."sm_emotions_id_seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_following_id_seq"
    OWNED BY "public"."sm_following"."id";
SELECT setval('"public"."sm_following_id_seq"', 34, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_journeys_id_seq"
    OWNED BY "public"."sm_journeys"."id";
SELECT setval('"public"."sm_journeys_id_seq"', 22, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_likes_id_seq"
    OWNED BY "public"."sm_likes"."id";
SELECT setval('"public"."sm_likes_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_paragraphs_id_seq"
    OWNED BY "public"."sm_paragraphs"."id";
SELECT setval('"public"."sm_paragraphs_id_seq"', 36, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_role_id_seq"
    OWNED BY "public"."sm_roles"."id";
SELECT setval('"public"."sm_role_id_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_story_posts_id_seq"
    OWNED BY "public"."sm_story_posts"."id";
SELECT setval('"public"."sm_story_posts_id_seq"', 47, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_story_type_id_seq"
    OWNED BY "public"."sm_story_type"."id";
SELECT setval('"public"."sm_story_type_id_seq"', 10, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_todo_id_seq"
    OWNED BY "public"."sm_todo"."id";
SELECT setval('"public"."sm_todo_id_seq"', 13, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_todo_lists_id_seq"
    OWNED BY "public"."sm_todo_lists"."id";
SELECT setval('"public"."sm_todo_lists_id_seq"', 15, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_user_role_id_seq"
    OWNED BY "public"."sm_user_roles"."id";
SELECT setval('"public"."sm_user_role_id_seq"', 34, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sm_users_id_seq"
    OWNED BY "public"."sm_users"."id";
SELECT setval('"public"."sm_users_id_seq"', 34, true);

-- ----------------------------
-- Primary Key structure for table sm_comment
-- ----------------------------
ALTER TABLE "public"."sm_comment" ADD CONSTRAINT "sm_comment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_emotions
-- ----------------------------
ALTER TABLE "public"."sm_emotions" ADD CONSTRAINT "sm_emotions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_following
-- ----------------------------
ALTER TABLE "public"."sm_following" ADD CONSTRAINT "sm_following_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_journeys
-- ----------------------------
ALTER TABLE "public"."sm_journeys" ADD CONSTRAINT "sm_journeys_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_likes
-- ----------------------------
ALTER TABLE "public"."sm_likes" ADD CONSTRAINT "sm_likes_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_paragraphs
-- ----------------------------
ALTER TABLE "public"."sm_paragraphs" ADD CONSTRAINT "sm_paragraphs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table sm_roles
-- ----------------------------
ALTER TABLE "public"."sm_roles" ADD CONSTRAINT "sm_role_name_key" UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table sm_roles
-- ----------------------------
ALTER TABLE "public"."sm_roles" ADD CONSTRAINT "sm_role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_story_posts
-- ----------------------------
ALTER TABLE "public"."sm_story_posts" ADD CONSTRAINT "sm_story_posts_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_story_type
-- ----------------------------
ALTER TABLE "public"."sm_story_type" ADD CONSTRAINT "sm_story_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_todo
-- ----------------------------
ALTER TABLE "public"."sm_todo" ADD CONSTRAINT "sm_todo_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_todo_lists
-- ----------------------------
ALTER TABLE "public"."sm_todo_lists" ADD CONSTRAINT "sm_todo_lists_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sm_user_roles
-- ----------------------------
ALTER TABLE "public"."sm_user_roles" ADD CONSTRAINT "sm_user_role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table sm_users
-- ----------------------------
ALTER TABLE "public"."sm_users" ADD CONSTRAINT "sm_users_email_key" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table sm_users
-- ----------------------------
ALTER TABLE "public"."sm_users" ADD CONSTRAINT "sm_users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table sm_comment
-- ----------------------------
ALTER TABLE "public"."sm_comment" ADD CONSTRAINT "fk_sm_story_posts" FOREIGN KEY ("story_id") REFERENCES "public"."sm_story_posts" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sm_comment" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_following
-- ----------------------------
ALTER TABLE "public"."sm_following" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_journeys
-- ----------------------------
ALTER TABLE "public"."sm_journeys" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_likes
-- ----------------------------
ALTER TABLE "public"."sm_likes" ADD CONSTRAINT "fk_sm_story_posts" FOREIGN KEY ("story_id") REFERENCES "public"."sm_story_posts" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sm_likes" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_paragraphs
-- ----------------------------
ALTER TABLE "public"."sm_paragraphs" ADD CONSTRAINT "fk_storys" FOREIGN KEY ("story_id") REFERENCES "public"."sm_story_posts" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_story_posts
-- ----------------------------
ALTER TABLE "public"."sm_story_posts" ADD CONSTRAINT "fk_sm_emotions" FOREIGN KEY ("emotion_id") REFERENCES "public"."sm_emotions" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sm_story_posts" ADD CONSTRAINT "fk_sm_story_type" FOREIGN KEY ("story_type_id") REFERENCES "public"."sm_story_type" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sm_story_posts" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_todo
-- ----------------------------
ALTER TABLE "public"."sm_todo" ADD CONSTRAINT "sm_todo_todolist_id_fkey" FOREIGN KEY ("todolist_id") REFERENCES "public"."sm_todo_lists" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_todo_lists
-- ----------------------------
ALTER TABLE "public"."sm_todo_lists" ADD CONSTRAINT "fk_sm_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table sm_user_roles
-- ----------------------------
ALTER TABLE "public"."sm_user_roles" ADD CONSTRAINT "fk_roles" FOREIGN KEY ("role_id") REFERENCES "public"."sm_roles" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sm_user_roles" ADD CONSTRAINT "fk_users" FOREIGN KEY ("user_id") REFERENCES "public"."sm_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
