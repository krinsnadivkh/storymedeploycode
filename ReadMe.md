**StoryMe API**


**Build with** Spring boot
**Use Open API**

**Installation**
1. Clone the repo

Git Clone https://gitlab.com/kshrd-10th-generation-ite/basic-course/storyme/storyme-api.git




**Start**

1. To start the project Go to file > Open... > choose the cloned project folder
2. Open Resource > Run schema.sql
3. Run Project by click on the start button
4. Open Browser and type (http://localhost:8080/swagger-ui/index.html#/) link open api


**Usage**


    Story me  is a Web application that free for user to read and create their 
story.

For more examples, Please prefer to the Demo

**What we have done**

-------------------------------------------------------------
------------------We have 7 Rest Controller------------------
-------------------------------------------------------------'
1. User account rest controller
   -Total end-point: 7

2. Authentication rest controller
   -Total end-point: 5

3. Story rest controller
   -Total end-point: 16

4. To do-list rest controller
   -Total end-point: 7

5. JOurney rest controller
   -Total end-point: 5

6. Admin rest controller
   -Total end-point: 14

7. File rest controller

   -Total end-point: 1

-------------------------------------------------------------
------------------Total End-point:55 ------------------
-------------------------------------------------------------'




1. Authenenticate-restcontroller :
- signUp
- loing wiht email and password
- forgot password
- verify Token (verify code which we have sent to email for permissio to change the password)
- reset password (create new password)

2. User-account-restcontroller:
- follow and unfollow
- update profile
- get user info
- get all following
- get all follower
- count total follower
- count total following

3. Story-restcontroller:
- request story type
- react story (like)
- create story  (each story may can have more than 1 paragraphs)
- create paragraphs
- create comment
- update privacy (private or public)
- update story
- update paragraphs
- search user by name
- search story by title
- search story by type
- get all story that user have post
- read more
- get react story
- get popular story
- delete story

4.To do-list-restcontroller
- create to do-list (in each to do-list it may can have more than 1 to do )
- create to do
- update to do-list
- update to do
- update complete action (in each to do user chage to they have done it)
- dete to do-list
- dete to do


5.Journey-restcontroller
- get all journey
- create journey
- update privacy
- update journey
- delete journey


5.Admin-restcontroller
- ban and unban user
- accept story-type request
- get total-type request
- search user by name
- get user info
- count total comment
- count total post
- get user story
- get all user
- get all story request
- get all


5.Fle-restcontroller
- upload single photo



